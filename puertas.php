<?php include 'include/header.php'; ?>
	<section class="row productos">
		<div class="col12 titulo">
			<h3>Puertas</h3>
		</div>
		<div class="col12">
			<h3>Puertas corredizas</h3>
		</div>
			<figure>
				<a href="#" data-popup-open="popup-pcorredizas01"><img src="images/pcorredizas01.jpg" alt=""></a>
				<figcaption>
					<h3>Puertas corredizas</h3>
					<a href="#" data-popup-open="popup-pcorredizas01">Ver más</a>
				</figcaption>
			</figure>
			<figure>
				<a href="#" data-popup-open="popup-pcorredizas02"><img src="images/pcorredizas02.jpg" alt=""></a>
				<figcaption>
					<h3>Puertas corredizas</h3>
					<a href="#" data-popup-open="popup-pcorredizas02">Ver más</a>
				</figcaption>
			</figure>
			<figure>
				<a href="#" data-popup-open="popup-pcorredizas03"><img src="images/pcorredizas03.jpg" alt=""></a>
				<figcaption>
					<h3>Puertas corredizas</h3>
					<a href="#" data-popup-open="popup-pcorredizas03">Ver más</a>
				</figcaption>
			</figure>
		<div class="col12">
			<h3>Puertas batientes</h3>
		</div>
			<figure>
				<a href="#" data-popup-open="popup-pbatientes01"><img src="images/pbatientes01.jpg" alt=""></a>
				<figcaption>
					<h3>Puertas batientes</h3>
					<a href="#" data-popup-open="popup-pbatientes01">Ver más</a>
				</figcaption>
			</figure>
			<figure>
				<a href="#" data-popup-open="popup-pbatientes02"><img src="images/pbatientes02.jpg" alt=""></a>
				<figcaption>
					<h3>Puertas batientes</h3>
					<a href="#" data-popup-open="popup-pbatientes02">Ver más</a>
				</figcaption>
			</figure>
			<figure>
				<a href="#" data-popup-open="popup-pbatientes03"><img src="images/pbatientes03.jpg" alt=""></a>
				<figcaption>
					<h3>Puertas batientes</h3>
					<a href="#" data-popup-open="popup-pbatientes03">Ver más</a>
				</figcaption>
			</figure>
	</section>
	<div class="modal" data-popup="popup-pcorredizas01">
		<div class="modal-content">
			<div class="modal-header">
				<a class="popup-back" href="#" data-popup-close="popup-pcorredizas01" data-popup-open="popup-pcorredizas03"><</a>
				<a class="popup-next" href="#" data-popup-close="popup-pcorredizas01" data-popup-open="popup-pcorredizas02">></a>
				<a class="popup-close" data-popup-close="popup-pcorredizas01" href="#">x</a>
				<h2>Puertas corredizas</h2>
			</div>
		    <div class="modal-body">
			    <section class="row poproductos">
			    	<div class="col col6">
			    		<img src="images/pcorredizas01.jpg" alt="">
			    	</div>
			    	<div class="col col6">
			    		<h4>Descripción</h4>
			    		<p>La puerta corredera está especialmente indicada cuando se dispone de un espacio reducido</p>
			    	</div>
			    </section>
		    </div>
		</div>
	</div>
	<div class="modal" data-popup="popup-pcorredizas02">
		<div class="modal-content">
			<div class="modal-header">
				<a class="popup-back" href="#" data-popup-open="popup-pcorredizas01" data-popup-close="popup-pcorredizas02"><</a>
				<a class="popup-next" href="#" data-popup-open="popup-pcorredizas03" data-popup-close="popup-pcorredizas02">></a>
				<a class="popup-close" data-popup-close="popup-pcorredizas02" href="#">x</a>
				<h2>Puertas corredizas</h2>
			</div>
		    <div class="modal-body">
			    <section class="row poproductos">
			    	<div class="col col6">
			    		<img src="images/pcorredizas02.jpg" alt="">
			    	</div>
			    	<div class="col col6">
			    		<h4>Descripción</h4>
			    		<p>La puerta corredera está especialmente indicada cuando se dispone de un espacio reducido</p>
			    	</div>
			    </section>
		    </div>
		</div>
	</div>
	<div class="modal" data-popup="popup-pcorredizas03">
		<div class="modal-content">
			<div class="modal-header">
				<a class="popup-back" href="#" data-popup-open="popup-pcorredizas02" data-popup-close="popup-pcorredizas03"><</a>
				<a class="popup-next" href="#" data-popup-open="popup-pcorredizas01" data-popup-close="popup-pcorredizas03">></a>
				<a class="popup-close" data-popup-close="popup-pcorredizas03" href="#">x</a>
				<h2>Puertas corredizas</h2>
			</div>
		    <div class="modal-body">
			    <section class="row poproductos">
			    	<div class="col col6">
			    		<img src="images/pcorredizas03.jpg" alt="">
			    	</div>
			    	<div class="col col6">
			    		<h4>Descripción</h4>
			    		<p>La puerta corredera está especialmente indicada cuando se dispone de un espacio reducido</p>
			    	</div>
			    </section>
		    </div>
		</div>
	</div>
	<div class="modal" data-popup="popup-pbatientes01">
		<div class="modal-content">
			<div class="modal-header">
				<a class="popup-back" href="#" data-popup-open="popup-pbatientes03" data-popup-close="popup-pbatientes01"><</a>
				<a class="popup-next" href="#" data-popup-open="popup-pbatientes02" data-popup-close="popup-pbatientes01">></a>
				<a class="popup-close" data-popup-close="popup-pbatientes01" href="#">x</a>
				<h2>Puertas batientes</h2>
			</div>
		    <div class="modal-body">
			    <section class="row poproductos">
			    	<div class="col col6">
			    		<img src="images/pbatientes01.jpg" alt="">
			    	</div>
			    	<div class="col col6">
			    		<h4>Descripción</h4>
			    		<p>La puerta corredera está especialmente indicada cuando se dispone de un espacio reducido</p>
			    	</div>
			    </section>
		    </div>
		</div>
	</div>
	<div class="modal" data-popup="popup-pbatientes02">
		<div class="modal-content">
			<div class="modal-header">
				<a class="popup-back" href="#" data-popup-open="popup-pbatientes01" data-popup-close="popup-pbatientes02"><</a>
				<a class="popup-next" href="#" data-popup-open="popup-pbatientes03" data-popup-close="popup-pbatientes02">></a>
				<a class="popup-close" data-popup-close="popup-pbatientes02" href="#">x</a>
				<h2>Puertas batientes</h2>
			</div>
		    <div class="modal-body">
			    <section class="row poproductos">
			    	<div class="col col6">
			    		<img src="images/pbatientes02.jpg" alt="">
			    	</div>
			    	<div class="col col6">
			    		<h4>Descripción</h4>
			    		<p>La puerta corredera está especialmente indicada cuando se dispone de un espacio reducido</p>
			    	</div>
			    </section>
		    </div>
		</div>
	</div>
	<div class="modal" data-popup="popup-pbatientes03">
		<div class="modal-content">
			<div class="modal-header">
				<a class="popup-back" href="#" data-popup-open="popup-pbatientes02" data-popup-close="popup-pbatientes03"><</a>
				<a class="popup-next" href="#" data-popup-open="popup-pbatientes01" data-popup-close="popup-pbatientes03">></a>
				<a class="popup-close" data-popup-close="popup-pbatientes03" href="#">x</a>
				<h2>Puertas batientes</h2>
			</div>
		    <div class="modal-body">
			    <section class="row poproductos">
			    	<div class="col col6">
			    		<img src="images/pbatientes03.jpg" alt="">
			    	</div>
			    	<div class="col col6">
			    		<h4>Descripción</h4>
			    		<p>La puerta corredera está especialmente indicada cuando se dispone de un espacio reducido</p>
			    	</div>
			    </section>
		    </div>
		</div>
	</div>
<?php include 'include/footer.php'; ?>
