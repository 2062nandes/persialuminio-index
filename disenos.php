<?php include 'include/header.php'; ?>
	<section class="row productos">
		<div class="col12 titulo">
			<h3>Diseños esmerilados</h3>
		</div>
		<div class="col12">
			<h3>Diseño esmerilado abstracto</h3>
		</div>
		<figure>
			<a href="#" data-popup-open="popup-desabstracto01"><img src="images/desabstracto01.jpg" alt=""></a>
			<figcaption>
				<h3>Diseño esmerilado abstracto</h3>
				<a href="#" data-popup-open="popup-desabstracto01">Ver más</a>
			</figcaption>
		</figure>
		<figure>
			<a href="#" data-popup-open="popup-desabstracto02"><img src="images/desabstracto02.jpg" alt=""></a>
			<figcaption>
				<h3>Diseño esmerilado abstracto</h3>
				<a href="#" data-popup-open="popup-desabstracto02">Ver más</a>
			</figcaption>
		</figure>
		<figure>
			<a href="#" data-popup-open="popup-desabstracto03"><img src="images/desabstracto03.jpg" alt=""></a>
			<figcaption>
				<h3>Diseño esmerilado abstracto</h3>
				<a href="#" data-popup-open="popup-desabstracto03">Ver más</a>
			</figcaption>
		</figure>
		<!--<div class="col12">
			<h3>Diseño esmerilado de rayas</h3>
		</div>
		<figure>
			<a href="#" data-popup-open="popup-desrayas01"><img src="images/desrayas01.jpg" alt=""></a>
			<figcaption>
				<h3>Diseño esmerilado de rayas</h3>
				<a href="#" data-popup-open="popup-desrayas01">Ver más</a>
			</figcaption>
		</figure>
		<figure>
			<a href="#" data-popup-open="popup-desrayas02"><img src="images/desrayas02.jpg" alt=""></a>
			<figcaption>
				<h3>Diseño esmerilado de rayas</h3>
				<a href="#" data-popup-open="popup-desrayas02">Ver más</a>
			</figcaption>
		</figure>
		<figure>
			<a href="#" data-popup-open="popup-desrayas03"><img src="images/desrayas03.jpg" alt=""></a>
			<figcaption>
				<h3>Diseño esmerilado de rayas</h3>
				<a href="#" data-popup-open="popup-desrayas03">Ver más</a>
			</figcaption>
		</figure>
		<div class="col12">
			<h3>Diseño esmerilado de dibujos</h3>
		</div>
		<figure>
			<a href="#" data-popup-open="popup-desdibujos01"><img src="images/desdibujos01.jpg" alt=""></a>
			<figcaption>
				<h3>Diseño esmerilado de dibujos</h3>
				<a href="#" data-popup-open="popup-desdibujos01">Ver más</a>
			</figcaption>
		</figure>
		<figure>
			<a href="#" data-popup-open="popup-desdibujos02"><img src="images/desdibujos02.jpg" alt=""></a>
			<figcaption>
				<h3>Diseño esmerilado de dibujos</h3>
				<a href="#" data-popup-open="popup-desdibujos02">Ver más</a>
			</figcaption>
		</figure>
		<figure>
			<a href="#" data-popup-open="popup-desdibujos03"><img src="images/desdibujos03.jpg" alt=""></a>
			<figcaption>
				<h3>Diseño esmerilado de dibujos</h3>
				<a href="#" data-popup-open="popup-desdibujos03">Ver más</a>
			</figcaption>
		</figure>
		<div class="col12">
			<h3>Diseño esmerilado de flores</h3>
		</div>
		<figure>
			<a href="#" data-popup-open="popup-desflores01"><img src="images/desflores01.jpg" alt=""></a>
			<figcaption>
				<h3>Diseño esmerilado de flores</h3>
				<a href="#" data-popup-open="popup-desflores01">Ver más</a>
			</figcaption>
		</figure>
		<figure>
			<a href="#" data-popup-open="popup-desflores02"><img src="images/desflores02.jpg" alt=""></a>
			<figcaption>
				<h3>Diseño esmerilado de flores</h3>
				<a href="#" data-popup-open="popup-desflores02">Ver más</a>
			</figcaption>
		</figure>
		<figure>
			<a href="#" data-popup-open="popup-desflores03"><img src="images/desflores03.jpg" alt=""></a>
			<figcaption>
				<h3>Diseño esmerilado de flores</h3>
				<a href="#" data-popup-open="popup-desflores03">Ver más</a>
			</figcaption>
		</figure>
		<div class="col12">
			<h3>Diseño esmerilado de letras</h3>
		</div>
		<figure>
			<a href="#" data-popup-open="popup-desletras01"><img src="images/desletras01.jpg" alt=""></a>
			<figcaption>
				<h3>Diseño esmerilado de letras</h3>
				<a href="#" data-popup-open="popup-desletras01">Ver más</a>
			</figcaption>
		</figure>
		<figure>
			<a href="#" data-popup-open="popup-desletras02"><img src="images/desletras02.jpg" alt=""></a>
			<figcaption>
				<h3>Diseño esmerilado de letras</h3>
				<a href="#" data-popup-open="popup-desletras02">Ver más</a>
			</figcaption>
		</figure>
		<figure>
			<a href="#" data-popup-open="popup-desletras03"><img src="images/desletras03.jpg" alt=""></a>
			<figcaption>
				<h3>Diseño esmerilado de letras</h3>
				<a href="#" data-popup-open="popup-desletras03">Ver más</a>
			</figcaption>
		</figure>
		<div class="col12">
			<h3>Diseño esmerilado de logotipos</h3>
		</div>
		<figure>
			<a href="#" data-popup-open="popup-deslogotipo01"><img src="images/deslogotipo01.jpg" alt=""></a>
			<figcaption>
				<h3>Diseño esmerilado de logotipos</h3>
				<a href="#" data-popup-open="popup-deslogotipo01">Ver más</a>
			</figcaption>
		</figure>
		<figure>
			<a href="#" data-popup-open="popup-deslogotipo02"><img src="images/deslogotipo02.jpg" alt=""></a>
			<figcaption>
				<h3>Diseño esmerilado de logotipos</h3>
				<a href="#" data-popup-open="popup-deslogotipo02">Ver más</a>
			</figcaption>
		</figure>
		<figure>
			<a href="#" data-popup-open="popup-deslogotipo03"><img src="images/deslogotipo03.jpg" alt=""></a>
			<figcaption>
				<h3>Diseño esmerilado de logotipos</h3>
				<a href="#" data-popup-open="popup-deslogotipo03">Ver más</a>
			</figcaption>
		</figure>
		<div class="col12">
			<h3>Diseño esmerilado de paisajes</h3>
		</div>
		<figure>
			<a href="#" data-popup-open="popup-despaisaje01"><img src="images/despaisaje01.jpg" alt=""></a>
			<figcaption>
				<h3>Diseño esmerilado de paisajes</h3>
				<a href="#" data-popup-open="popup-despaisaje01">Ver más</a>
			</figcaption>
		</figure>
		<figure>
			<a href="#" data-popup-open="popup-despaisaje02"><img src="images/despaisaje02.jpg" alt=""></a>
			<figcaption>
				<h3>Diseño esmerilado de paisajes</h3>
				<a href="#" data-popup-open="popup-despaisaje02">Ver más</a>
			</figcaption>
		</figure>
		<figure>
			<a href="#" data-popup-open="popup-despaisaje03"><img src="images/despaisaje03.jpg" alt=""></a>
			<figcaption>
				<h3>Diseño esmerilado de paisajes</h3>
				<a href="#" data-popup-open="popup-despaisaje03">Ver más</a>
			</figcaption>
		</figure>-->
	</section>
	<!--
    Diseño esmerilado abstracto
	Diseño esmerilado de rayas [desrayas]
	Diseño esmerilado de dibujos [desdibujos]
	Diseño esmerilado de flores [desflores]
	Diseño esmerilado de letras []
	Diseño esmerilado de logotipos
	Diseño esmerilado de paisajes
 -->
	<div class="modal" data-popup="popup-desmerilados01">
		<div class="modal-content">
			<div class="modal-header">
				<a class="popup-back" href="#" data-popup-open="popup-desabstracto03" data-popup-close="popup-desabstracto01"><</a>
				<a class="popup-next" href="#" data-popup-open="popup-desabstracto02" data-popup-close="popup-desabstracto01">></a>
				<a class="popup-close" data-popup-close="popup-desabstracto01" href="#">x</a>
				<h2>Diseños esmerilados</h2>
			</div>
		    <div class="modal-body">
			    <section class="row poproductos">
			    	<div class="col col6">
			    		<img src="images/desabstracto01.jpg" alt="">
			    	</div>
			    	<div class="col col6">
			    		<h4>Descripción</h4>
			    		<p>Imagen en las paredes o en el mismo esmerilado de los vidrios brindando un ambiente unificado.</p>
			    	</div>
			    </section>
		    </div>
		</div>
	</div>
	<div class="modal" data-popup="popup-desabstracto02">
		<div class="modal-content">
			<div class="modal-header">
				<a class="popup-back" href="#" data-popup-open="popup-desabstracto01" data-popup-close="popup-desabstracto02"><</a>
				<a class="popup-next" href="#" data-popup-open="popup-desabstracto03" data-popup-close="popup-desabstracto02">></a>
				<a class="popup-close" data-popup-close="popup-desabstracto02" href="#">x</a>
				<h2>Diseños esmerilados</h2>
			</div>
		    <div class="modal-body">
			    <section class="row poproductos">
			    	<div class="col col6">
			    		<img src="images/desabstracto02.jpg" alt="">
			    	</div>
			    	<div class="col col6">
			    		<h4>Descripción</h4>
			    		<p>Imagen en las paredes o en el mismo esmerilado de los vidrios brindando un ambiente unificado.</p>
			    	</div>
			    </section>
		    </div>
		</div>
	</div>
	<div class="modal" data-popup="popup-desabstracto03">
		<div class="modal-content">
			<div class="modal-header">
				<a class="popup-back" href="#" data-popup-open="popup-desabstracto02" data-popup-close="popup-desabstracto03"><</a>
				<a class="popup-next" href="#" data-popup-open="popup-desabstracto01" data-popup-close="popup-desabstracto03">></a>
				<a class="popup-close" data-popup-close="popup-desabstracto03" href="#">x</a>
				<h2>Diseños esmerilados</h2>
			</div>
		    <div class="modal-body">
			    <section class="row poproductos">
			    	<div class="col col6">
			    		<img src="images/desabstracto03.jpg" alt="">
			    	</div>
			    	<div class="col col6">
			    		<h4>Descripción</h4>
			    		<p>Imagen en las paredes o en el mismo esmerilado de los vidrios brindando un ambiente unificado.</p>
			    	</div>
			    </section>
		    </div>
		</div>
	</div>
<?php include 'include/footer.php'; ?>
