<?php include 'include/header.php'; ?>
	<section class="row productos">
		<div class="col12">
			<h3>Boxes de aluminio con acrílico</h3>
		</div>
		<figure>
			<img src="images/box-aluminio-01.jpg" alt="">
			<figcaption>
				<h3>Box de aluminio con acrilico</h3>
				<a href="#" data-popup-open="popup-a01">Ver más</a>
			</figcaption>
		</figure>
		<figure>
			<img src="images/box-aluminio-02.jpg" alt="">
			<figcaption>
				<h3>Box de aluminio con acrilico</h3>
				<a href="#" data-popup-open="popup-a02">Ver más</a>
			</figcaption>
		</figure>
	</section>
	<div class="modal" data-popup="popup-a01">
		<div class="modal-content">
			<div class="modal-header">
				<a class="popup-close" data-popup-close="popup-a01" href="#">x</a>
				<h2>Boxes de aluminio con acrílico</h2>
			</div>
		    <div class="modal-body">
			    <section class="row poproductos">
			    	<div class="col col6">
			    		<img src="images/box-aluminio-01.jpg" alt="">
			    	</div>
			    	<div class="col col6">
			    		<h4>Descripción</h4>
			    		<p>El acrílico caja y espesor de 2 mm.</p>
						<p>La caja estándar cuando acrílico es 1,90 m.</p>
						<p>Caja de aluminio en acrílico se puede fabricar en los siguientes modelos:</p>
						<p>Box aluminio en acrílico frontal (recta)</p>
						<p>caja de aluminio en la esquina de acrílico ( "L")</p>
						<p>caja de aluminio en acrílico en el baño (altura 1,50 m)</p>
						<p>El acrílico para la caja de aluminio tiene la opción de los colores:</p>
						<p>Incoloro, verde, bronce, ahumado y trabajó, el tipo de puntos.</p>
			    	</div>
			    </section>
		    </div>
		</div>
	</div>
	<div class="modal" data-popup="popup-a02">
		<div class="modal-content">
			<div class="modal-header">
				<a class="popup-close" data-popup-close="popup-a02" href="#">x</a>
				<h2>Boxes de aluminio con acrílico</h2>
			</div>
		    <div class="modal-body">
			    <section class="row poproductos">
			    	<div class="col col6">
			    		<img src="images/box-aluminio-02.jpg" alt="">
			    	</div>
			    	<div class="col col6">
			    		<h4>Descripción</h4>
			    		<p>El acrílico caja y espesor de 2 mm.</p>
						<p>La caja estándar cuando acrílico es 1,90 m.</p>
						<p>Caja de aluminio en acrílico se puede fabricar en los siguientes modelos:</p>
						<p>Box aluminio en acrílico frontal (recta)</p>
						<p>caja de aluminio en la esquina de acrílico ( "L")</p>
						<p>caja de aluminio en acrílico en el baño (altura 1,50 m)</p>
						<p>El acrílico para la caja de aluminio tiene la opción de los colores:</p>
						<p>Incoloro, verde, bronce, ahumado y trabajó, el tipo de puntos.</p>
			    	</div>
			    </section>
		    </div>
		</div>
	</div>
<?php include 'include/footer.php'; ?>	