<?php include 'include/header.php'; ?>
	<section class="row productos">
		<div class="col12">
			<h3>Espejos biselados</h3>
		</div>
			<figure>
				<img src="images/espejos01.jpg" alt="">
				<figcaption>
					<h3>Espejos</h3>
					<a href="#" data-popup-open="popup-espejos01">Ver más</a>
				</figcaption>
			</figure>
			<figure>
				<img src="images/espejos02.jpg" alt="">
				<figcaption>
					<h3>Espejos</h3>
					<a href="#" data-popup-open="popup-espejos02">Ver más</a>
				</figcaption>
			</figure>
			<figure>
				<img src="images/espejos03.jpg" alt="">
				<figcaption>
					<h3>Espejos</h3>
					<a href="#" data-popup-open="popup-espejos03">Ver más</a>
				</figcaption>
			</figure>
	</section>
	<div class="modal" data-popup="popup-espejos01">
		<div class="modal-content">
			<div class="modal-header">
				<a class="popup-close" data-popup-close="popup-espejos01" href="#">x</a>
				<h2>Espejos biselados</h2>
			</div>
		    <div class="modal-body">
			    <section class="row poproductos">
			    	<div class="col col6">
			    		<img src="images/espejos01.jpg" alt="">
			    	</div>
			    	<div class="col col6">
			    		<h4>Descripción</h4>
			    		<p>Lo que caracteriza a este modelo es el elemento embellecedor o tapeta que cubre las fijaciones del vidrio.</p>
			    	</div>
			    </section>
		    </div>
		</div>
	</div>
	<div class="modal" data-popup="popup-espejos02">
		<div class="modal-content">
			<div class="modal-header">
				<a class="popup-close" data-popup-close="popup-espejos02" href="#">x</a>
				<h2>Espejos biselados</h2>
			</div>
		    <div class="modal-body">
			    <section class="row poproductos">
			    	<div class="col col6">
			    		<img src="images/espejos02.jpg" alt="">
			    	</div>
			    	<div class="col col6">
			    		<h4>Descripción</h4>
			    		<p>Lo que caracteriza a este modelo es el elemento embellecedor o tapeta que cubre las fijaciones del vidrio.</p>
			    	</div>
			    </section>
		    </div>
		</div>
	</div>
	<div class="modal" data-popup="popup-espejos03">
		<div class="modal-content">
			<div class="modal-header">
				<a class="popup-close" data-popup-close="popup-espejos03" href="#">x</a>
				<h2>Espejos biselados</h2>
			</div>
		    <div class="modal-body">
			    <section class="row poproductos">
			    	<div class="col col6">
			    		<img src="images/espejos03.jpg" alt="">
			    	</div>
			    	<div class="col col6">
			    		<h4>Descripción</h4>
			    		<p>Lo que caracteriza a este modelo es el elemento embellecedor o tapeta que cubre las fijaciones del vidrio.</p>
			    	</div>
			    </section>
		    </div>
		</div>
	</div>
<?php include 'include/footer.php'; ?>
