<?php include 'include/header.php'; ?>
	<section class="row productos">
		<div class="col12 titulo">
			<h3>Techos interiores</h3>
		</div>
		<div class="col12">
			<h3>Cielos falsos de plastoformo</h3>
		</div>
			<figure>
				<a href="#" data-popup-open="popup-tplastoformo01"><img src="images/tplastoformo01.jpg" alt=""></a>
				<figcaption>
					<h3>Cielos falsos de plastoformo</h3>
					<a href="#" data-popup-open="popup-tplastoformo01">Ver más</a>
				</figcaption>
			</figure>
			<figure>
				<a href="#" data-popup-open="popup-tplastoformo02"><img src="images/tplastoformo02.jpg" alt=""></a>
				<figcaption>
					<h3>Cielos falsos de plastoformo</h3>
					<a href="#" data-popup-open="popup-tplastoformo02">Ver más</a>
				</figcaption>
			</figure>
			<figure>
				<a href="#" data-popup-open="popup-tplastoformo03"><img src="images/tplastoformo03.jpg" alt=""></a>
				<figcaption>
					<h3>Cielos falsos de plastoformo</h3>
					<a href="#" data-popup-open="popup-tplastoformo03">Ver más</a>
				</figcaption>
			</figure>
		<!-- <div class="col12">
			<h3>Cielos falsos de estuco</h3>
		</div>
			<figure>
				<a href="#" data-popup-open="popup-testuco01"><img src="images/testuco01.jpg" alt=""></a>
				<figcaption>
					<h3>Cielos falsos de estuco</h3>
					<a href="#" data-popup-open="popup-testuco01">Ver más</a>
				</figcaption>
			</figure>
			<figure>
				<a href="#" data-popup-open="popup-testuco02"><img src="images/testuco02.jpg" alt=""></a>
				<figcaption>
					<h3>Cielos falsos de estuco</h3>
					<a href="#" data-popup-open="popup-testuco02">Ver más</a>
				</figcaption>
			</figure>
			<figure>
				<a href="#" data-popup-open="popup-testuco03"><img src="images/testuco03.jpg" alt=""></a>
				<figcaption>
					<h3>Cielos falsos de estuco</h3>
					<a href="#" data-popup-open="popup-testuco03">Ver más</a>
				</figcaption>
			</figure> -->
		<div class="col12">
			<h3>Cielos falsos de PVC</h3>
		</div>
			<figure>
				<a href="#" data-popup-open="popup-tpvc01"><img src="images/tpvc01.jpg" alt=""></a>
				<figcaption>
					<h3>Cielos falsos de PVC</h3>
					<a href="#" data-popup-open="popup-tpvc01">Ver más</a>
				</figcaption>
			</figure>
			<figure>
				<a href="#" data-popup-open="popup-tpvc02"><img src="images/tpvc02.jpg" alt=""></a>
				<figcaption>
					<h3>Cielos falsos de PVC</h3>
					<a href="#" data-popup-open="popup-tpvc02">Ver más</a>
				</figcaption>
			</figure>
			<figure>
				<a href="#" data-popup-open="popup-tpvc03"><img src="images/tpvc03.jpg" alt=""></a>
				<figcaption>
					<h3>Cielos falsos de PVC</h3>
					<a href="#" data-popup-open="popup-tpvc03">Ver más</a>
				</figcaption>
			</figure>
		<div class="col12 titulo">
			<h3>Techos exteriores</h3>
		</div>
		<div class="col12">
			<h3>Techos de vidrio</h3>
		</div>
			<figure>
				<a href="#" data-popup-open="popup-tvidrio01"><img src="images/tvidrio01.jpg" alt=""></a>
				<figcaption>
					<h3>Techos de vidrio</h3>
					<a href="#" data-popup-open="popup-tvidrio01">Ver más</a>
				</figcaption>
			</figure>
			<figure>
				<a href="#" data-popup-open="popup-tvidrio02"><img src="images/tvidrio02.jpg" alt=""></a>
				<figcaption>
					<h3>Techos de vidrio</h3>
					<a href="#" data-popup-open="popup-tvidrio02">Ver más</a>
				</figcaption>
			</figure>
			<figure>
				<a href="#" data-popup-open="popup-tvidrio03"><img src="images/tvidrio03.jpg" alt=""></a>
				<figcaption>
					<h3>Techos de vidrio</h3>
					<a href="#" data-popup-open="popup-tvidrio03">Ver más</a>
				</figcaption>
			</figure>
		<div class="col12">
			<h3>Techos de policarbonato</h3>
		</div>
			<figure>
				<a href="#" data-popup-open="popup-tpolicarbonato01"><img src="images/tpolicarbonato01.jpg" alt=""></a>
				<figcaption>
					<h3>Techos de policarbonato</h3>
					<a href="#" data-popup-open="popup-tpolicarbonato01">Ver más</a>
				</figcaption>
			</figure>
			<figure>
				<a href="#" data-popup-open="popup-tpolicarbonato02"><img src="images/tpolicarbonato02.jpg" alt=""></a>
				<figcaption>
					<h3>Techos de policarbonato</h3>
					<a href="#" data-popup-open="popup-tpolicarbonato02">Ver más</a>
				</figcaption>
			</figure>
			<figure>
				<a href="#" data-popup-open="popup-tpolicarbonato03"><img src="images/tpolicarbonato03.jpg" alt=""></a>
				<figcaption>
					<h3>Techos de policarbonato</h3>
					<a href="#" data-popup-open="popup-tpolicarbonato03">Ver más</a>
				</figcaption>
			</figure>
			<!--
	   - Techos interiores
                  - Cielos falsos de plastoformo
                  - Cielos falsos de estuco
                  - Cielos falsos de PVC
       - Techos exteriores
                  - Techos de vidrio
                  - Techos de policarbonato-->
	</section>
	<div class="modal" data-popup="popup-tplastoformo01">
		<div class="modal-content">
			<div class="modal-header">
				<a class="popup-back" href="#" data-popup-open="popup-tplastoformo03" data-popup-close="popup-tplastoformo01"><</a>
				<a class="popup-next" href="#" data-popup-open="popup-tplastoformo02" data-popup-close="popup-tplastoformo01">></a>
				<a class="popup-close" data-popup-close="popup-tplastoformo01" href="#">x</a>
				<h2>Cielos falsos de plastoformo</h2>
			</div>
		    <div class="modal-body">
			    <section class="row poproductos">
			    	<div class="col col6">
			    		<img src="images/tplastoformo01.jpg" alt="">
			    	</div>
			    	<div class="col col6">
			    		<h4>Descripción</h4>
			    		<p>Los techos plastoformo se presentan como una solución perfecta.</p>
			    	</div>
			    </section>
		    </div>
		</div>
	</div>
	<div class="modal" data-popup="popup-tplastoformo02">
		<div class="modal-content">
			<div class="modal-header">
				<a class="popup-back" href="#" data-popup-open="popup-tplastoformo01" data-popup-close="popup-tplastoformo02"><</a>
				<a class="popup-next" href="#" data-popup-open="popup-tplastoformo03" data-popup-close="popup-tplastoformo02">></a>
				<a class="popup-close" data-popup-close="popup-tplastoformo02" href="#">x</a>
				<h2>Cielos falsos de plastoformo</h2>
			</div>
		    <div class="modal-body">
			    <section class="row poproductos">
			    	<div class="col col6">
			    		<img src="images/tplastoformo02.jpg" alt="">
			    	</div>
			    	<div class="col col6">
			    		<h4>Descripción</h4>
			    		<p>Los techos plastoformo se presentan como una solución perfecta.</p>
			    	</div>
			    </section>
		    </div>
		</div>
	</div>
	<div class="modal" data-popup="popup-tplastoformo03">
		<div class="modal-content">
			<div class="modal-header">
				<a class="popup-back" href="#" data-popup-open="popup-tplastoformo02" data-popup-close="popup-tplastoformo03"><</a>
				<a class="popup-next" href="#" data-popup-open="popup-tplastoformo01" data-popup-close="popup-tplastoformo03">></a>
				<a class="popup-close" data-popup-close="popup-tplastoformo03" href="#">x</a>
				<h2>Cielos falsos de plastoformo</h2>
			</div>
		    <div class="modal-body">
			    <section class="row poproductos">
			    	<div class="col col6">
			    		<img src="images/tplastoformo03.jpg" alt="">
			    	</div>
			    	<div class="col col6">
			    		<h4>Descripción</h4>
			    		<p>Los techos plastoformo se presentan como una solución perfecta.</p>
			    	</div>
			    </section>
		    </div>
		</div>
	</div>
	<div class="modal" data-popup="popup-testuco01">
		<div class="modal-content">
			<div class="modal-header">
				<a class="popup-back" href="#" data-popup-open="popup-testuco03" data-popup-close="popup-testuco01"><</a>
				<a class="popup-next" href="#" data-popup-open="popup-testuco02" data-popup-close="popup-testuco01">></a>
				<a class="popup-close" data-popup-close="popup-testuco01" href="#">x</a>
				<h2>Cielos falsos de estuco</h2>
			</div>
		    <div class="modal-body">
			    <section class="row poproductos">
			    	<div class="col col6">
			    		<img src="images/testuco01.jpg" alt="">
			    	</div>
			    	<div class="col col6">
			    		<h4>Descripción</h4>
			    		<p>Los techos estuco se presentan como una solución perfecta.</p>
			    	</div>
			    </section>
		    </div>
		</div>
	</div>
	<div class="modal" data-popup="popup-testuco02">
		<div class="modal-content">
			<div class="modal-header">
				<a class="popup-back" href="#" data-popup-open="popup-testuco01" data-popup-close="popup-testuco02"><</a>
				<a class="popup-next" href="#" data-popup-open="popup-testuco03" data-popup-close="popup-testuco02">></a>
				<a class="popup-close" data-popup-close="popup-testuco02" href="#">x</a>
				<h2>Cielos falsos de estuco</h2>
			</div>
		    <div class="modal-body">
			    <section class="row poproductos">
			    	<div class="col col6">
			    		<img src="images/testuco02.jpg" alt="">
			    	</div>
			    	<div class="col col6">
			    		<h4>Descripción</h4>
			    		<p>Los techos estuco se presentan como una solución perfecta.</p>
			    	</div>
			    </section>
		    </div>
		</div>
	</div>
	<div class="modal" data-popup="popup-testuco03">
		<div class="modal-content">
			<div class="modal-header">
				<a class="popup-back" href="#" data-popup-open="popup-testuco02" data-popup-close="popup-testuco03"><</a>
				<a class="popup-next" href="#" data-popup-open="popup-testuco01" data-popup-close="popup-testuco03">></a>
				<a class="popup-close" data-popup-close="popup-testuco03" href="#">x</a>
				<h2>Cielos falsos de estuco</h2>
			</div>
		    <div class="modal-body">
			    <section class="row poproductos">
			    	<div class="col col6">
			    		<img src="images/testuco03.jpg" alt="">
			    	</div>
			    	<div class="col col6">
			    		<h4>Descripción</h4>
			    		<p>Los techos estuco se presentan como una solución perfecta.</p>
			    	</div>
			    </section>
		    </div>
		</div>
	</div>
	<div class="modal" data-popup="popup-tpvc01">
		<div class="modal-content">
			<div class="modal-header">
				<a class="popup-back" href="#" data-popup-open="popup-tpvc03" data-popup-close="popup-tpvc01"><</a>
				<a class="popup-next" href="#" data-popup-open="popup-tpvc02" data-popup-close="popup-tpvc01">></a>
				<a class="popup-close" data-popup-close="popup-tpvc01" href="#">x</a>
				<h2>Cielos falsos de pvc</h2>
			</div>
		    <div class="modal-body">
			    <section class="row poproductos">
			    	<div class="col col6">
			    		<img src="images/tpvc01.jpg" alt="">
			    	</div>
			    	<div class="col col6">
			    		<h4>Descripción</h4>
			    		<p>Los techos pvc se presentan como una solución perfecta.</p>
			    	</div>
			    </section>
		    </div>
		</div>
	</div>
	<div class="modal" data-popup="popup-tpvc02">
		<div class="modal-content">
			<div class="modal-header">
				<a class="popup-back" href="#" data-popup-open="popup-tpvc01" data-popup-close="popup-tpvc02"><</a>
				<a class="popup-next" href="#" data-popup-open="popup-tpvc03" data-popup-close="popup-tpvc02">></a>
				<a class="popup-close" data-popup-close="popup-tpvc02" href="#">x</a>
				<h2>Cielos falsos de pvc</h2>
			</div>
		    <div class="modal-body">
			    <section class="row poproductos">
			    	<div class="col col6">
			    		<img src="images/tpvc02.jpg" alt="">
			    	</div>
			    	<div class="col col6">
			    		<h4>Descripción</h4>
			    		<p>Los techos pvc se presentan como una solución perfecta.</p>
			    	</div>
			    </section>
		    </div>
		</div>
	</div>
	<div class="modal" data-popup="popup-tpvc03">
		<div class="modal-content">
			<div class="modal-header">
				<a class="popup-back" href="#" data-popup-open="popup-tpvc02" data-popup-close="popup-tpvc03"><</a>
				<a class="popup-next" href="#" data-popup-open="popup-tpvc01" data-popup-close="popup-tpvc03">></a>
				<a class="popup-close" data-popup-close="popup-tpvc03" href="#">x</a>
				<h2>Cielos falsos de pvc</h2>
			</div>
		    <div class="modal-body">
			    <section class="row poproductos">
			    	<div class="col col6">
			    		<img src="images/tpvc03.jpg" alt="">
			    	</div>
			    	<div class="col col6">
			    		<h4>Descripción</h4>
			    		<p>Los techos pvc se presentan como una solución perfecta.</p>
			    	</div>
			    </section>
		    </div>
		</div>
	</div>
	<div class="modal" data-popup="popup-tvidrio01">
		<div class="modal-content">
			<div class="modal-header">
				<a class="popup-back" href="#" data-popup-open="popup-tvidrio03" data-popup-close="popup-tvidrio01"><</a>
				<a class="popup-next" href="#" data-popup-open="popup-tvidrio02" data-popup-close="popup-tvidrio01">></a>
				<a class="popup-close" data-popup-close="popup-tvidrio01" href="#">x</a>
				<h2>Techos de vidrio</h2>
			</div>
		    <div class="modal-body">
			    <section class="row poproductos">
			    	<div class="col col6">
			    		<img src="images/tvidrio01.jpg" alt="">
			    	</div>
			    	<div class="col col6">
			    		<h4>Descripción</h4>
			    		<p>Los techos vidrio se presentan como una solución perfecta.</p>
			    	</div>
			    </section>
		    </div>
		</div>
	</div>
	<div class="modal" data-popup="popup-tvidrio02">
		<div class="modal-content">
			<div class="modal-header">
				<a class="popup-back" href="#" data-popup-open="popup-tvidrio01" data-popup-close="popup-tvidrio02"><</a>
				<a class="popup-next" href="#" data-popup-open="popup-tvidrio03" data-popup-close="popup-tvidrio02">></a>
				<a class="popup-close" data-popup-close="popup-tvidrio02" href="#">x</a>
				<h2>Techos de vidrio</h2>
			</div>
		    <div class="modal-body">
			    <section class="row poproductos">
			    	<div class="col col6">
			    		<img src="images/tvidrio02.jpg" alt="">
			    	</div>
			    	<div class="col col6">
			    		<h4>Descripción</h4>
			    		<p>Los techos vidrio se presentan como una solución perfecta.</p>
			    	</div>
			    </section>
		    </div>
		</div>
	</div>
	<div class="modal" data-popup="popup-tvidrio03">
		<div class="modal-content">
			<div class="modal-header">
				<a class="popup-back" href="#" data-popup-open="popup-tvidrio02" data-popup-close="popup-tvidrio03"><</a>
				<a class="popup-next" href="#" data-popup-open="popup-tvidrio01" data-popup-close="popup-tvidrio03">></a>
				<a class="popup-close" data-popup-close="popup-tvidrio03" href="#">x</a>
				<h2>Techos de vidrio</h2>
			</div>
		    <div class="modal-body">
			    <section class="row poproductos">
			    	<div class="col col6">
			    		<img src="images/tvidrio03.jpg" alt="">
			    	</div>
			    	<div class="col col6">
			    		<h4>Descripción</h4>
			    		<p>Los techos vidrio se presentan como una solución perfecta.</p>
			    	</div>
			    </section>
		    </div>
		</div>
	</div>
	<div class="modal" data-popup="popup-tpolicarbonato01">
		<div class="modal-content">
			<div class="modal-header">
				<a class="popup-back" href="#" data-popup-open="popup-tpolicarbonato03" data-popup-close="popup-tpolicarbonato01"><</a>
				<a class="popup-next" href="#" data-popup-open="popup-tpolicarbonato02" data-popup-close="popup-tpolicarbonato01">></a>
				<a class="popup-close" data-popup-close="popup-tpolicarbonato01" href="#">x</a>
				<h2>Techos de policarbonato</h2>
			</div>
		    <div class="modal-body">
			    <section class="row poproductos">
			    	<div class="col col6">
			    		<img src="images/tpolicarbonato01.jpg" alt="">
			    	</div>
			    	<div class="col col6">
			    		<h4>Descripción</h4>
			    		<p>Los techos policarbonato se presentan como una solución perfecta.</p>
			    	</div>
			    </section>
		    </div>
		</div>
	</div>
	<div class="modal" data-popup="popup-tpolicarbonato02">
		<div class="modal-content">
			<div class="modal-header">
				<a class="popup-back" href="#" data-popup-open="popup-tpolicarbonato01" data-popup-close="popup-tpolicarbonato02"><</a>
				<a class="popup-next" href="#" data-popup-open="popup-tpolicarbonato03" data-popup-close="popup-tpolicarbonato02">></a>
				<a class="popup-close" data-popup-close="popup-tpolicarbonato02" href="#">x</a>
				<h2>Techos de policarbonato</h2>
			</div>
		    <div class="modal-body">
			    <section class="row poproductos">
			    	<div class="col col6">
			    		<img src="images/tpolicarbonato02.jpg" alt="">
			    	</div>
			    	<div class="col col6">
			    		<h4>Descripción</h4>
			    		<p>Los techos policarbonato se presentan como una solución perfecta.</p>
			    	</div>
			    </section>
		    </div>
		</div>
	</div>
	<div class="modal" data-popup="popup-tpolicarbonato03">
		<div class="modal-content">
			<div class="modal-header">
				<a class="popup-back" href="#" data-popup-open="popup-tpolicarbonato02" data-popup-close="popup-tpolicarbonato03"><</a>
				<a class="popup-next" href="#" data-popup-open="popup-tpolicarbonato01" data-popup-close="popup-tpolicarbonato03">></a>
				<a class="popup-close" data-popup-close="popup-tpolicarbonato03" href="#">x</a>
				<h2>Techos de policarbonato</h2>
			</div>
		    <div class="modal-body">
			    <section class="row poproductos">
			    	<div class="col col6">
			    		<img src="images/tpolicarbonato03.jpg" alt="">
			    	</div>
			    	<div class="col col6">
			    		<h4>Descripción</h4>
			    		<p>Los techos policarbonato se presentan como una solución perfecta.</p>
			    	</div>
			    </section>
		    </div>
		</div>
	</div>
<?php include 'include/footer.php'; ?>
