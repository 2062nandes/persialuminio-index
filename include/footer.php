<?php // include 'index2.php'; ?>
	<article class="productos contactos">
		<div class="col12">
			<h3>Contáctenos</h3>
		</div>
		<div id="app">
				<div class="row">
					<form v-on:submit.prevent="submitForm" method="post" id="theForm" class="form second" action="mail.php" role="form">
						<div class="col6">
							<div class="row">
								<label class="radio-option"><input type="radio" v-model="vue.exp" :value="true"> SOLICITO COTIZACIÓN ("ya tengo medidas")</label>
								<label class="radio-option"><input type="radio" v-model="vue.exp" :value="false"> SOLICITO MEDICIÓN A DOMICILIO (Es gratuita)</label>
							</div>
						 		<input v-model="vue.nombre" type="text" id="nombre" class="validate form-input nombre"  name="nombre" placeholder="Nombre Completo..." required>
						 		<input v-model="vue.telefono" type="number" id="telefono" class="validate form-input fono" name="telefono" placeholder="Número de Telefóno..." required>
								<input v-model="vue.movil" type="number" id="movil" class="validate form-input cel" name="movil" placeholder="Número Telefóno Móvil..." required>
								<input v-model="vue.ciudad" type="text" id="ciudad" class="validate form-input ciudad" name="ciudad" placeholder="Ciudad..." required>
							<div v-if="vue.exp">
								<input v-model="vue.requeridos" type="text" id="requeridos" class="validate form-input" placeholder="Productos requeridos..." required>
								<input v-model="vue.medidas" type="text" id="medidas" class="validate form-input" name="" value="" placeholder="Medidas... 00x00" required>
							</div>

						</div>
						<div class="col6">
							<input v-model="vue.direccion" type="text" id="direccion" class="validate form-input dire" name="direccion" placeholder="Dirección... " required>
		          <input v-model="vue.email" type="email" id="email" class="validate form-input email" name="email" placeholder="Email" required>
		          <textarea v-model="vue.mensaje" id="mensaje" class="form-textarea mensaje" name="mensaje" cols="35" rows="5" required placeholder="Mensaje..."></textarea>
		          <button type="submit" class="submitbtn form-submit" value="Enviar" v-bind:disabled="!isFormValid()">Enviar</button>
							<button v-on:click="clearForm()" type="reset" class="form-submit" v-bind:disabled="!isFormValid()">Limpiar</button>
						</div>
		      </form>
				</div>
				<div v-if="formSubmitted" id="statusMessage1">
					<h4>{{ vue.envio }}</h4>
				</div>
			</div>
	</article>
	<article class="productos">
			<div class="col12">
				<h3>Mapa de ubicación</h3>
			</div>
			<object class="mapa_persialuminio" type="text/html" data="mapa-persialuminio/index.html"></object>
	</article>
	<footer class="row">
		<div class="col col4">
			<h3>Productos</h3>
			<nav>
				<a href="boxes.php"><i class="fa fa-angle-right" aria-hidden="true"></i>Boxes de aluminio</a>
				<a href="boxes.php"><i class="fa fa-angle-right" aria-hidden="true"></i>Boxes de blindex</a>
				<a href="puertas.php"><i class="fa fa-angle-right" aria-hidden="true"></i>Puertas y ventanas</a>
				<a href="mallasmosquiteras.php"><i class="fa fa-angle-right" aria-hidden="true"></i>Mallas mosquiteras</a>
				<a href="techos.php"><i class="fa fa-angle-right" aria-hidden="true"></i>Techos</a>
				<a href="mampara.php"><i class="fa fa-angle-right" aria-hidden="true"></i>Mamparas</a>
				<a href="persianas.php"><i class="fa fa-angle-right" aria-hidden="true"></i>Persianas</a>
				<a href="fachadas.php"><i class="fa fa-angle-right" aria-hidden="true"></i>Fachadas estructurales</a>
				<a href="espejos.php"><i class="fa fa-angle-right" aria-hidden="true"></i>Espejos biselados</a>
			</nav>
		</div>
		<div class="col col4">
			<h3>Dirección</h3>
			<p>SANTA CRUZ,  Zona Los Ángeles</p>
			<p> Av. Paraguá # 2505 esq. Tarechi, entre 2º y 3º Anillo
				<!--<a href="contactos.php"> >>Ver mapa</a>-->
			</p>
			<p>Telfs.: 3461359 – 3465259 - 3489569 </p>
			<p> Cel. 71300042 <i class="fa fa-whatsapp" aria-hidden="true"></i></p>
			<p>persialuminio@gmail.com</p>
		</div>
		<div class="col col4">
			<h3>PERSIALUMINIO<sup>©</sup></h3>
			<h5>Todos los Derechos Reservados<sup>®</sup>2017</h5>
			<h3>Síguenos en:</h3>
			<div class="social">
				<a href=""><i class="fa fa-facebook" aria-hidden="true"></i></a>
				<a href=""><i class="fa fa-twitter" aria-hidden="true"></i></a>
				<a href=""><i class="fa fa-google-plus-official" aria-hidden="true"></i></a>
				<a href=""><i class="fa fa-youtube" aria-hidden="true"></i></a>
			</div>
			<p>Diseño y programación: <a href="http://www.ahpublic.com">AH! PUBLICIDAD</a></p>
		</div>
	</footer>
</section>
</body>
</html>
