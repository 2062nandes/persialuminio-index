<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<title>PERSIALUMINIO. Carpintería de aluminio, vidrios blindex y estructurales</title>
	<meta name="description" content="PERSIALUMIO. Carpintería de aluminio, boxes de aluminio con acrílico, boxes de blindex esmerilado con diseño, puertas y ventanas de aluminio y blindex, mallas mosquiteras, techos de vidrio y policarbonato, divisiones de ambientes o mamparas, persianas verticales y horizontales, fachadas estructurales, espejos biselados, perfiles de aluminio y accesorios">
	<meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1">
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, minimum-scale=1, maximum-scale=1">
	<meta name="designer" content="Leonardo Calle - leonard_rua@hotmail.com">
	<!--[if lt IE 9]>
		<script src="js/html5shiv.min.js"></script>
		<script src="js/respond.min.js"></script>
	<![endif]-->
	<link rel="shortcut icon" href="favicon.ico">
	<link rel="stylesheet" type="text/css" href="engine1/style.css" />
	<link rel="stylesheet" href="css/style.css">
	<script src="js/prefixfree.min.js"></script>
	<script src="js/jquery.min.js"></script>
	<script src="js/vue.js"></script>
	<script src="js/vue-resource.min.js"></script>
	<script src="js/script.js"></script>
</head>
<body>
<section class="container">
	<header class="row">
		<div class="col3">
			<div class="emblema hidden">
				<img src="images/emblema.png" alt="">
			</div>
		</div>
		<div class="col6">
			<img src="images/logo.png" alt="Logotipo PERSIALUMIO">
		</div>
		<nav class="col3">
			<a href="./">Inicio<i class="fa fa-home" aria-hidden="true"></i></a>
			<a href="#">Nosotros<i class="fa fa-user" aria-hidden="true"></i></a>
			<a href="contactos.php">Contactos<i class="fa fa-envelope-o" aria-hidden="true"></i></a>
		</nav>
	</header>
	<nav class="row nav">
		<a href="puertas.php">Puertas</a>
		<a href="ventanas.php">Ventanas</a>
		<a href="boxes.php">Boxes y Espejos</a>
		<a href="fachadas.php">Fachadas y Barandas</a>
		<a href="techos.php">Techos</a>
		<a href="mampara.php">Mamparas</a>
		<a href="persianas.php">Persianas</a>
		<a href="disenos.php">Diseños Esmerilados</a>
		<a href="mallasmosquiteras.php">Mallas Mosquiteras</a>
	<!--
		<a href="box-aluminio.php">Boxes de aluminio</a>
		<a href="box-blindex.php">Boxes de blindex</a>
		<a href="puertasyventanas.php">Puertas y ventanas</a>
		<a href="mallasmosquiteras.php">Mallas mosquiteras</a>
		<a href="techos.php">Techos</a>
		<a href="mampara.php">Mamparas</a>
		<a href="persianas.php">Persianas</a>
		<a href="fachadas.php">Fachadas estructurales</a>
		<a href="espejos.php">Espejos biselados</a>-->
	</nav>
