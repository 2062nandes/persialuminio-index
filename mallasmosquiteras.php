<?php include 'include/header.php'; ?>
	<section class="row productos">
		<div class="col12">
			<h3>Mallas mosquiteras</h3>
		</div>
			<figure>
				<a href="#" data-popup-open="popup-mallas01"><img src="images/mallas01.jpg" alt=""></a>
				<figcaption>
					<h3>Mallas Ventanas</h3>
					<a href="#" data-popup-open="popup-mallas01">Ver más</a>
				</figcaption>
			</figure>
			<figure>
				<a href="#" data-popup-open="popup-mallas02"><img src="images/mallas02.jpg" alt=""></a>
				<figcaption>
					<h3>Mallas Ventanas</h3>
					<a href="#" data-popup-open="popup-mallas02">Ver más</a>
				</figcaption>
			</figure>
			<figure>
				<a href="#" data-popup-open="popup-mallas03"><img src="images/mallas03.jpg" alt=""></a>
				<figcaption>
					<h3>Mallas Ventanas</h3>
					<a href="#" data-popup-open="popup-mallas03">Ver más</a>
				</figcaption>
			</figure>
	</section>
	<div class="modal" data-popup="popup-mallas01">
		<div class="modal-content">
			<div class="modal-header">
				<a class="popup-back" href="#" data-popup-open="popup-mallas03" data-popup-close="popup-mallas01"><</a>
				<a class="popup-next" href="#" data-popup-open="popup-mallas02" data-popup-close="popup-mallas01">></a>
				<a class="popup-close" data-popup-close="popup-mallas01" href="#">x</a>
				<h2>Mallas mosquiteras</h2>
			</div>
		    <div class="modal-body">
			    <section class="row poproductos">
			    	<div class="col col6">
			    		<img src="images/mallas01.jpg" alt="">
			    	</div>
			    	<div class="col col6">
			    		<h4>Descripción</h4>
			    		<p>Mallas Mosquiteras en puertas y ventanas de casas, apartamentos o fincas. Con esta solución las personas tendrán la tranquilidad y el confort de estar en el interior de la vivienda sabiendo que los molestos zancudos, mosquitos, murciélagos, chapolas, cucarrones, cucarachas voladoras y todo tipo de animales o insectos voladores, no van a ingresar. </p>
			    	</div>
			    </section>
		    </div>
		</div>
	</div>
	<div class="modal" data-popup="popup-mallas02">
		<div class="modal-content">
			<div class="modal-header">
				<a class="popup-back" href="#" data-popup-open="popup-mallas01" data-popup-close="popup-mallas02"><</a>
				<a class="popup-next" href="#" data-popup-open="popup-mallas03" data-popup-close="popup-mallas02">></a>
				<a class="popup-close" data-popup-close="popup-mallas02" href="#">x</a>
				<h2>Mallas mosquiteras</h2>
			</div>
		    <div class="modal-body">
			    <section class="row poproductos">
			    	<div class="col col6">
			    		<img src="images/mallas02.jpg" alt="">
			    	</div>
			    	<div class="col col6">
			    		<h4>Descripción</h4>
			    		<p>Mallas Mosquiteras en puertas y ventanas de casas, apartamentos o fincas. Con esta solución las personas tendrán la tranquilidad y el confort de estar en el interior de la vivienda sabiendo que los molestos zancudos, mosquitos, murciélagos, chapolas, cucarrones, cucarachas voladoras y todo tipo de animales o insectos voladores, no van a ingresar. </p>
			    	</div>
			    </section>
		    </div>
		</div>
	</div>
	<div class="modal" data-popup="popup-mallas03">
		<div class="modal-content">
			<div class="modal-header">
				<a class="popup-back" href="#" data-popup-open="popup-mallas02" data-popup-close="popup-mallas03"><</a>
				<a class="popup-next" href="#" data-popup-open="popup-mallas01" data-popup-close="popup-mallas03">></a>
				<a class="popup-close" data-popup-close="popup-mallas03" href="#">x</a>
				<h2>Mallas mosquiteras</h2>
			</div>
		    <div class="modal-body">
			    <section class="row poproductos">
			    	<div class="col col6">
			    		<img src="images/mallas03.jpg" alt="">
			    	</div>
			    	<div class="col col6">
			    		<h4>Descripción</h4>
			    		<p>Mallas Mosquiteras en puertas y ventanas de casas, apartamentos o fincas. Con esta solución las personas tendrán la tranquilidad y el confort de estar en el interior de la vivienda sabiendo que los molestos zancudos, mosquitos, murciélagos, chapolas, cucarrones, cucarachas voladoras y todo tipo de animales o insectos voladores, no van a ingresar. </p>
			    	</div>
			    </section>
		    </div>
		</div>
	</div>
<?php include 'include/footer.php'; ?>
