<?php include 'include/header.php'; ?>
	<section class="row slider">
		<div id="wowslider-container1">
		<div class="ws_images"><ul>
				<li><img src="data1/images/slide-1.jpg" alt="" title="" id="wows1_0"/></li>
				<li><img src="data1/images/slide-2.jpg" alt="" title="" id="wows1_1"/></li>
				<li><img src="data1/images/slide-3.jpg" alt="" title="" id="wows1_2"/></li>
				<li><img src="data1/images/slide-4.jpg" alt="" title="" id="wows1_3"/></li>
				<li><img src="data1/images/slide-5.jpg" alt="" title="" id="wows1_4"/></li>
				<li><img src="data1/images/slide-6.jpg" alt="" title="" id="wows1_5"/></li>
				<li><img src="data1/images/slide-7.jpg" alt="" title="" id="wows1_5"/></li>
				<li><img src="data1/images/slide-8.jpg" alt="" title="" id="wows1_5"/></li>
				<li><img src="data1/images/slide-9.jpg" alt="" title="" id="wows1_5"/></li>
			</ul></div>
			<div class="ws_bullets"><div>
				<a href="#" title=""><span><img src="data1/tooltips/slider01.jpg" alt=""/>1</span></a>
				<a href="#" title=""><span><img src="data1/tooltips/slider02.jpg" alt=""/>2</span></a>
				<a href="#" title=""><span><img src="data1/tooltips/slider03.jpg" alt=""/>3</span></a>
				<a href="#" title=""><span><img src="data1/tooltips/slider04.jpg" alt=""/>4</span></a>
				<a href="#" title=""><span><img src="data1/tooltips/slider05.jpg" alt=""/>5</span></a>
				<a href="#" title=""><span><img src="data1/tooltips/slider06.jpg" alt=""/>6</span></a>
			</div></div>
		<div class="ws_shadow"></div>
		</div>
		<script type="text/javascript" src="engine1/wowslider.js"></script>
		<script type="text/javascript" src="engine1/script.js"></script>
	</section>
	<section class="row soluciones">
		<div class="col col12">
			<h2>TE OFRECEMOS LAS MEJORES SOLUCIONES</h2>
		</div>
		<figure class="col com5 col4 solucion">
			<a href="boxes.php"><img src="images/servicio01.jpg" alt=""></a>
			<figcaption>
				<h4>Boxes de aluminio con acrílico</h4>
				<p>La Boxes de acrílico es un modelo más barato, en comparación con la Boxes de vidrio templado, su estructura (perfiles) son placa de aluminio y la placa de acrílico.</p>
				<a href="boxes.php">Ver producto</a>
			</figcaption>
		</figure>
		<figure class="col com5 col4 solucion">
			<a href="disenos.php"><img src="images/servicio02.jpg" alt=""></a>
			<figcaption>
				<h4>Boxes de blindex esmerilado con diseño</h4>
				<p> Se utiliza en boxes de oficina brindando privacidad entre ambientes y a la vez permitiendo el paso de luz natural o artificial.</p>
				<a href="disenos.php">Ver producto</a>
			</figcaption>
		</figure>
		<figure class="col com5 col4 solucion">
			<a href="puertas.php"><img src="images/servicio03.jpg" alt=""></a>
			<figcaption>
				<h4>Puertas y ventanas de aluminio y blindex</h4>
				<p>Disponemos de una amplia variedad de modelos de puertas y ventanas en aluminio que garantizan gran estanqueidad y aislamiento para tu vivienda.</p>
				<a href="ventanas.php">Ver producto</a>
			</figcaption>
		</figure>
		<figure class="col com5 col4 solucion">
			<a href="mallasmosquiteras.php"><img src="images/servicio04.jpg" alt=""></a>
			<figcaption>
				<h4>Mallas mosquiteras</h4>
				<p>Proteja su salud y la de los suyos, instale mallas contra insectos en las ventanas y puertas de su casa </p>
				<a href="mallasmosquiteras.php">Ver producto</a>
			</figcaption>
		</figure>
		<figure class="col com5 col4 solucion">
			<a href="techos.php"><img src="images/servicio05.jpg" alt=""></a>
			<figcaption>
				<h4>Techos de vidrio y policarbonato</h4>
				<p>Techos realizado totalmente con la mas alta calidad en materia prima y tecnología.</p>
				<a href="techos.php">Ver producto</a>
			</figcaption>
		</figure>
		<figure class="col com5 col4 solucion">
			<a href="mampara.php"><img src="images/servicio06.jpg" alt=""></a>
			<figcaption>
				<h4>Divisiones de ambientes o mamparas</h4>
				<p>Las mamparas para oficinas son elementos ideales para llevar a cabo la separación de espacios de trabajo de una forma sencilla, rápida y funcional.</p>
				<a href="mampara.php">Ver producto</a>
			</figcaption>
		</figure>
		<figure class="col com5 col4 solucion">
			<a href="persianas.php"><img src="images/servicio07.jpg" alt=""></a>
			<figcaption>
				<h4>Persianas verticales y horizontales</h4>
				<p>Persianas con una gran variedad texturas, screen, traslúcidas y black out, telas acrílicas y manuales o motorizadas.</p>
				<a href="persianas.php">Ver producto</a>
			</figcaption>
		</figure>
		<figure class="col com5 col4 solucion">
			<a href="fachadas.php"><img src="images/servicio08.jpg" alt=""></a>
			<figcaption>
				<h4>Fachadas estructurales</h4>
				<p>El sistema de vidrio estructural es una técnica de sujección del acristalamiento mediante el empleo de siliconas especiales.</p>
				<a href="fachadas.php">Ver producto</a>
			</figcaption>
		</figure>
		<figure class="col com5 col4 solucion">
			<a href="espejos.php"><img src="images/servicio09.jpg" alt=""></a>
			<figcaption>
				<h4>Espejos biselados</h4>
				<p>Decora tu hogar y oficina. Diseños y formatos personalizados según necesidad. Biselados, a medida, muros espejados, espejos de pared.</p>
				<a href="espejos.php">Ver producto</a>
			</figcaption>
		</figure>
	</section>
<?php include 'include/footer.php'; ?>
