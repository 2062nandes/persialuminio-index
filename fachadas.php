<?php include 'include/header.php'; ?>
	<section class="row productos">
		<div class="col12">
			<h3>Fachadas de blindex con arañas de inox</h3>
		</div>
			<figure>
				<a href="#" data-popup-open="popup-faranas01"><img src="images/faranas01.jpg" alt=""></a>
				<figcaption>
					<h3>Fachadas de blindex</h3>
					<a href="#" data-popup-open="popup-faranas01">Ver más</a>
				</figcaption>
			</figure>
			<figure>
				<a href="#" data-popup-open="popup-faranas02"><img src="images/faranas02.jpg" alt=""></a>
				<figcaption>
					<h3>Fachadas de blindex</h3>
					<a href="#" data-popup-open="popup-faranas02">Ver más</a>
				</figcaption>
			</figure>
			<figure>
				<a href="#" data-popup-open="popup-faranas03"><img src="images/faranas03.jpg" alt=""></a>
				<figcaption>
					<h3>Fachadas de blindex</h3>
					<a href="#" data-popup-open="popup-faranas03">Ver más</a>
				</figcaption>
			</figure>
		<div class="col12">
			<h3>Fachadas de vidrio flotante reflectivo</h3>
		</div>
			<figure>
				<a href="#" data-popup-open="popup-freflectivo01"><img src="images/freflectivo01.jpg" alt=""></a>
				<figcaption>
					<h3>Flotante reflectivo</h3>
					<a href="#" data-popup-open="popup-freflectivo01">Ver más</a>
				</figcaption>
			</figure>
			<figure>
				<a href="#" data-popup-open="popup-freflectivo02"><img src="images/freflectivo02.jpg" alt=""></a>
				<figcaption>
					<h3>Flotante reflectivo</h3>
					<a href="#" data-popup-open="popup-freflectivo02">Ver más</a>
				</figcaption>
			</figure>
			<figure>
				<a href="#" data-popup-open="popup-freflectivo03"><img src="images/freflectivo03.jpg" alt=""></a>
				<figcaption>
					<h3>Flotante reflectivo</h3>
					<a href="#" data-popup-open="popup-freflectivo03">Ver más</a>
				</figcaption>
			</figure>
		<div class="col12">
			<h3>Fachadas de vidrio con estructura de tubo metálico</h3>
		</div>
			<figure>
				<a href="#" data-popup-open="popup-festructuratubo01"><img src="images/festructuratubo01.jpg" alt=""></a>
				<figcaption>
					<h3>Estructura de tubo metálico</h3>
					<a href="#" data-popup-open="popup-festructuratubo01">Ver más</a>
				</figcaption>
			</figure>
			<figure>
				<a href="#" data-popup-open="popup-festructuratubo02"><img src="images/festructuratubo02.jpg" alt=""></a>
				<figcaption>
					<h3>Estructura de tubo metálico</h3>
					<a href="#" data-popup-open="popup-festructuratubo02">Ver más</a>
				</figcaption>
			</figure>
			<figure>
				<a href="#" data-popup-open="popup-festructuratubo03"><img src="images/festructuratubo03.jpg" alt=""></a>
				<figcaption>
					<h3>Estructura de tubo metálico</h3>
					<a href="#" data-popup-open="popup-festructuratubo03">Ver más</a>
				</figcaption>
			</figure>
		<div class="col12">
			<h3>Barandas de aluminio</h3>
		</div>
			<figure>
				<a href="#" data-popup-open="popup-baluminio01"><img src="images/baluminio01.jpg" alt=""></a>
				<figcaption>
					<h3>Barandas de aluminio</h3>
					<a href="#" data-popup-open="popup-baluminio01">Ver más</a>
				</figcaption>
			</figure>
			<figure>
				<a href="#" data-popup-open="popup-baluminio02"><img src="images/baluminio02.jpg" alt=""></a>
				<figcaption>
					<h3>Barandas de aluminio</h3>
					<a href="#" data-popup-open="popup-baluminio02">Ver más</a>
				</figcaption>
			</figure>
			<figure>
				<a href="#" data-popup-open="popup-baluminio03"><img src="images/baluminio03.jpg" alt=""></a>
				<figcaption>
					<h3>Barandas de aluminio</h3>
					<a href="#" data-popup-open="popup-baluminio03">Ver más</a>
				</figcaption>
			</figure>
		<div class="col12">
			<h3>Barandas de aluminio con blindex</h3>
		</div>
			<figure>
				<a href="#" data-popup-open="popup-baluminioblindex01"><img src="images/baluminioblindex01.jpg" alt=""></a>
				<figcaption>
					<h3>Barandas de aluminio con blindex</h3>
					<a href="#" data-popup-open="popup-baluminioblindex01">Ver más</a>
				</figcaption>
			</figure>
			<figure>
				<a href="#" data-popup-open="popup-baluminioblindex02"><img src="images/baluminioblindex02.jpg" alt=""></a>
				<figcaption>
					<h3>Barandas de aluminio con blindex</h3>
					<a href="#" data-popup-open="popup-baluminioblindex02">Ver más</a>
				</figcaption>
			</figure>
			<figure>
				<a href="#" data-popup-open="popup-baluminioblindex03"><img src="images/baluminioblindex03.jpg" alt=""></a>
				<figcaption>
					<h3>Barandas de aluminio con blindex</h3>
					<a href="#" data-popup-open="popup-baluminioblindex03">Ver más</a>
				</figcaption>
			</figure>
		<div class="col12">
			<h3>Barandas de acero inoxidable</h3>
		</div>
			<figure>
				<a href="#" data-popup-open="popup-bacero01"><img src="images/bacero01.jpg" alt=""></a>
				<figcaption>
					<h3>Barandas de acero inoxidable</h3>
					<a href="#" data-popup-open="popup-bacero01">Ver más</a>
				</figcaption>
			</figure>
			<figure>
				<a href="#" data-popup-open="popup-bacero02"><img src="images/bacero02.jpg" alt=""></a>
				<figcaption>
					<h3>Barandas de acero inoxidable</h3>
					<a href="#" data-popup-open="popup-bacero02">Ver más</a>
				</figcaption>
			</figure>
			<figure>
				<a href="#" data-popup-open="popup-bacero03"><img src="images/bacero03.jpg" alt=""></a>
				<figcaption>
					<h3>Barandas de acero inoxidable</h3>
					<a href="#" data-popup-open="popup-bacero03">Ver más</a>
				</figcaption>
			</figure>
	</section>
	<div class="modal" data-popup="popup-faranas01">
		<div class="modal-content">
			<div class="modal-header">
				<a class="popup-back" href="#" data-popup-open="popup-faranas03" data-popup-close="popup-faranas01"><</a>
				<a class="popup-next" href="#" data-popup-open="popup-faranas02" data-popup-close="popup-faranas01">></a>
				<a class="popup-close" data-popup-close="popup-faranas01" href="#">x</a>
				<h2>Fachadas de blindex</h2>
			</div>
		    <div class="modal-body">
			    <section class="row poproductos">
			    	<div class="col col6">
			    		<img src="images/faranas01.jpg" alt="">
			    	</div>
			    	<div class="col col6">
			    		<h4>Descripción</h4>
			    		<p>Lo que caracteriza a este modelo es el elemento embellecedor o tapeta que cubre las fijaciones del vidrio.</p>
			    	</div>
			    </section>
		    </div>
		</div>
	</div>
	<div class="modal" data-popup="popup-faranas02">
		<div class="modal-content">
			<div class="modal-header">
				<a class="popup-back" href="#" data-popup-open="popup-faranas01" data-popup-close="popup-faranas02"><</a>
				<a class="popup-next" href="#" data-popup-open="popup-faranas03" data-popup-close="popup-faranas02">></a>
				<a class="popup-close" data-popup-close="popup-faranas02" href="#">x</a>
				<h2>Fachadas de blindex</h2>
			</div>
		    <div class="modal-body">
			    <section class="row poproductos">
			    	<div class="col col6">
			    		<img src="images/faranas02.jpg" alt="">
			    	</div>
			    	<div class="col col6">
			    		<h4>Descripción</h4>
			    		<p>Lo que caracteriza a este modelo es el elemento embellecedor o tapeta que cubre las fijaciones del vidrio.</p>
			    	</div>
			    </section>
		    </div>
		</div>
	</div>
	<div class="modal" data-popup="popup-faranas03">
		<div class="modal-content">
			<div class="modal-header">
				<a class="popup-back" href="#" data-popup-open="popup-faranas02" data-popup-close="popup-faranas03"><</a>
				<a class="popup-next" href="#" data-popup-open="popup-faranas01" data-popup-close="popup-faranas03">></a>
				<a class="popup-close" data-popup-close="popup-faranas03" href="#">x</a>
				<h2>Fachadas de blindex</h2>
			</div>
		    <div class="modal-body">
			    <section class="row poproductos">
			    	<div class="col col6">
			    		<img src="images/faranas03.jpg" alt="">
			    	</div>
			    	<div class="col col6">
			    		<h4>Descripción</h4>
			    		<p>Lo que caracteriza a este modelo es el elemento embellecedor o tapeta que cubre las fijaciones del vidrio.</p>
			    	</div>
			    </section>
		    </div>
		</div>
	</div>
	<div class="modal" data-popup="popup-freflectivo01">
		<div class="modal-content">
			<div class="modal-header">
				<a class="popup-back" href="#" data-popup-open="popup-freflectivo03" data-popup-close="popup-freflectivo01"><</a>
				<a class="popup-next" href="#" data-popup-open="popup-freflectivo02" data-popup-close="popup-freflectivo01">></a>
				<a class="popup-close" data-popup-close="popup-freflectivo01" href="#">x</a>
				<h2>Fachadas de vidrio flotante reflectivo</h2>
			</div>
		    <div class="modal-body">
			    <section class="row poproductos">
			    	<div class="col col6">
			    		<img src="images/freflectivo01.jpg" alt="">
			    	</div>
			    	<div class="col col6">
			    		<h4>Descripción</h4>
			    		<p>Lo que caracteriza a este modelo es el elemento embellecedor o tapeta que cubre las fijaciones del vidrio.</p>
			    	</div>
			    </section>
		    </div>
		</div>
	</div>
	<div class="modal" data-popup="popup-freflectivo02">
		<div class="modal-content">
			<div class="modal-header">
				<a class="popup-back" href="#" data-popup-open="popup-freflectivo01" data-popup-close="popup-freflectivo02"><</a>
				<a class="popup-next" href="#" data-popup-open="popup-freflectivo03" data-popup-close="popup-freflectivo02">></a>
				<a class="popup-close" data-popup-close="popup-freflectivo02" href="#">x</a>
				<h2>Fachadas de vidrio flotante reflectivo</h2>
			</div>
		    <div class="modal-body">
			    <section class="row poproductos">
			    	<div class="col col6">
			    		<img src="images/freflectivo02.jpg" alt="">
			    	</div>
			    	<div class="col col6">
			    		<h4>Descripción</h4>
			    		<p>Lo que caracteriza a este modelo es el elemento embellecedor o tapeta que cubre las fijaciones del vidrio.</p>
			    	</div>
			    </section>
		    </div>
		</div>
	</div>
	<div class="modal" data-popup="popup-freflectivo03">
		<div class="modal-content">
			<div class="modal-header">
				<a class="popup-back" href="#" data-popup-open="popup-freflectivo02" data-popup-close="popup-freflectivo03"><</a>
				<a class="popup-next" href="#" data-popup-open="popup-freflectivo01" data-popup-close="popup-freflectivo03">></a>
				<a class="popup-close" data-popup-close="popup-freflectivo03" href="#">x</a>
				<h2>Fachadas de vidrio flotante reflectivo</h2>
			</div>
		    <div class="modal-body">
			    <section class="row poproductos">
			    	<div class="col col6">
			    		<img src="images/freflectivo03.jpg" alt="">
			    	</div>
			    	<div class="col col6">
			    		<h4>Descripción</h4>
			    		<p>Lo que caracteriza a este modelo es el elemento embellecedor o tapeta que cubre las fijaciones del vidrio.</p>
			    	</div>
			    </section>
		    </div>
		</div>
	</div>
	<div class="modal" data-popup="popup-festructuratubo01">
		<div class="modal-content">
			<div class="modal-header">
				<a class="popup-back" href="#" data-popup-open="popup-festructuratubo03" data-popup-close="popup-festructuratubo01"><</a>
				<a class="popup-next" href="#" data-popup-open="popup-festructuratubo02" data-popup-close="popup-festructuratubo01">></a>
				<a class="popup-close" data-popup-close="popup-festructuratubo01" href="#">x</a>
				<h2>Estructura de tubo metálico</h2>
			</div>
		    <div class="modal-body">
			    <section class="row poproductos">
			    	<div class="col col6">
			    		<img src="images/festructuratubo01.jpg" alt="">
			    	</div>
			    	<div class="col col6">
			    		<h4>Descripción</h4>
			    		<p>Lo que caracteriza a este modelo es el elemento embellecedor o tapeta que cubre las fijaciones del vidrio.</p>
			    	</div>
			    </section>
		    </div>
		</div>
	</div>
	<div class="modal" data-popup="popup-festructuratubo02">
		<div class="modal-content">
			<div class="modal-header">
				<a class="popup-back" href="#" data-popup-open="popup-festructuratubo01" data-popup-close="popup-festructuratubo02"><</a>
				<a class="popup-next" href="#" data-popup-open="popup-festructuratubo03" data-popup-close="popup-festructuratubo02">></a>
				<a class="popup-close" data-popup-close="popup-festructuratubo02" href="#">x</a>
				<h2>Estructura de tubo metálico</h2>
			</div>
		    <div class="modal-body">
			    <section class="row poproductos">
			    	<div class="col col6">
			    		<img src="images/festructuratubo02.jpg" alt="">
			    	</div>
			    	<div class="col col6">
			    		<h4>Descripción</h4>
			    		<p>Lo que caracteriza a este modelo es el elemento embellecedor o tapeta que cubre las fijaciones del vidrio.</p>
			    	</div>
			    </section>
		    </div>
		</div>
	</div>
	<div class="modal" data-popup="popup-festructuratubo03">
		<div class="modal-content">
			<div class="modal-header">
				<a class="popup-back" href="#" data-popup-open="popup-festructuratubo02" data-popup-close="popup-festructuratubo03"><</a>
				<a class="popup-next" href="#" data-popup-open="popup-festructuratubo01" data-popup-close="popup-festructuratubo03">></a>
				<a class="popup-close" data-popup-close="popup-festructuratubo03" href="#">x</a>
				<h2>Estructura de tubo metálico</h2>
			</div>
		    <div class="modal-body">
			    <section class="row poproductos">
			    	<div class="col col6">
			    		<img src="images/festructuratubo03.jpg" alt="">
			    	</div>
			    	<div class="col col6">
			    		<h4>Descripción</h4>
			    		<p>Lo que caracteriza a este modelo es el elemento embellecedor o tapeta que cubre las fijaciones del vidrio.</p>
			    	</div>
			    </section>
		    </div>
		</div>
	</div>
	<div class="modal" data-popup="popup-baluminio01">
		<div class="modal-content">
			<div class="modal-header">
				<a class="popup-back" href="#" data-popup-open="popup-baluminio03" data-popup-close="popup-baluminio01"><</a>
				<a class="popup-next" href="#" data-popup-open="popup-baluminio02" data-popup-close="popup-baluminio01">></a>
				<a class="popup-close" data-popup-close="popup-baluminio01" href="#">x</a>
				<h2>Barandas de aluminio</h2>
			</div>
		    <div class="modal-body">
			    <section class="row poproductos">
			    	<div class="col col6">
			    		<img src="images/baluminio01.jpg" alt="">
			    	</div>
			    	<div class="col col6">
			    		<h4>Descripción</h4>
			    		<p>Un estilo y belleza únicos. Es un producto moderno e innovador</p>
			    	</div>
			    </section>
		    </div>
		</div>
	</div>
	<div class="modal" data-popup="popup-baluminio02">
		<div class="modal-content">
			<div class="modal-header">
				<a class="popup-back" href="#" data-popup-open="popup-baluminio01" data-popup-close="popup-baluminio02"><</a>
				<a class="popup-next" href="#" data-popup-open="popup-baluminio03" data-popup-close="popup-baluminio02">></a>
				<a class="popup-close" data-popup-close="popup-baluminio02" href="#">x</a>
				<h2>Barandas de aluminio</h2>
			</div>
		    <div class="modal-body">
			    <section class="row poproductos">
			    	<div class="col col6">
			    		<img src="images/baluminio02.jpg" alt="">
			    	</div>
			    	<div class="col col6">
			    		<h4>Descripción</h4>
			    		<p>Un estilo y belleza únicos. Es un producto moderno e innovador</p>
			    	</div>
			    </section>
		    </div>
		</div>
	</div>
	<div class="modal" data-popup="popup-baluminio03">
		<div class="modal-content">
			<div class="modal-header">
				<a class="popup-back" href="#" data-popup-open="popup-baluminio02" data-popup-close="popup-baluminio03"><</a>
				<a class="popup-next" href="#" data-popup-open="popup-baluminio01" data-popup-close="popup-baluminio03">></a>
				<a class="popup-close" data-popup-close="popup-baluminio03" href="#">x</a>
				<h2>Barandas de aluminio</h2>
			</div>
		    <div class="modal-body">
			    <section class="row poproductos">
			    	<div class="col col6">
			    		<img src="images/baluminio03.jpg" alt="">
			    	</div>
			    	<div class="col col6">
			    		<h4>Descripción</h4>
			    		<p>Un estilo y belleza únicos. Es un producto moderno e innovador</p>
			    	</div>
			    </section>
		    </div>
		</div>
	</div>
	<div class="modal" data-popup="popup-baluminioblindex01">
		<div class="modal-content">
			<div class="modal-header">
				<a class="popup-back" href="#" data-popup-open="popup-baluminioblindex03" data-popup-close="popup-baluminioblindex01"><</a>
				<a class="popup-next" href="#" data-popup-open="popup-baluminioblindex02" data-popup-close="popup-baluminioblindex01">></a>
				<a class="popup-close" data-popup-close="popup-baluminioblindex01" href="#">x</a>
				<h2>Barandas de aluminio con blindex</h2>
			</div>
		    <div class="modal-body">
			    <section class="row poproductos">
			    	<div class="col col6">
			    		<img src="images/baluminioblindex01.jpg" alt="">
			    	</div>
			    	<div class="col col6">
			    		<h4>Descripción</h4>
			    		<p>Un estilo y belleza únicos. Es un producto moderno e innovador</p>
			    	</div>
			    </section>
		    </div>
		</div>
	</div>
	<div class="modal" data-popup="popup-baluminioblindex02">
		<div class="modal-content">
			<div class="modal-header">
				<a class="popup-back" href="#" data-popup-open="popup-baluminioblindex01" data-popup-close="popup-baluminioblindex02"><</a>
				<a class="popup-next" href="#" data-popup-open="popup-baluminioblindex03" data-popup-close="popup-baluminioblindex02">></a>
				<a class="popup-close" data-popup-close="popup-baluminioblindex02" href="#">x</a>
				<h2>Barandas de aluminio con blindex</h2>
			</div>
		    <div class="modal-body">
			    <section class="row poproductos">
			    	<div class="col col6">
			    		<img src="images/baluminioblindex02.jpg" alt="">
			    	</div>
			    	<div class="col col6">
			    		<h4>Descripción</h4>
			    		<p>Un estilo y belleza únicos. Es un producto moderno e innovador</p>
			    	</div>
			    </section>
		    </div>
		</div>
	</div>
	<div class="modal" data-popup="popup-baluminioblindex03">
		<div class="modal-content">
			<div class="modal-header">
				<a class="popup-back" href="#" data-popup-open="popup-baluminioblindex02" data-popup-close="popup-baluminioblindex03"><</a>
				<a class="popup-next" href="#" data-popup-open="popup-baluminioblindex01" data-popup-close="popup-baluminioblindex03">></a>
				<a class="popup-close" data-popup-close="popup-baluminioblindex03" href="#">x</a>
				<h2>Barandas de aluminio con blindex</h2>
			</div>
		    <div class="modal-body">
			    <section class="row poproductos">
			    	<div class="col col6">
			    		<img src="images/baluminioblindex03.jpg" alt="">
			    	</div>
			    	<div class="col col6">
			    		<h4>Descripción</h4>
			    		<p>Un estilo y belleza únicos. Es un producto moderno e innovador</p>
			    	</div>
			    </section>
		    </div>
		</div>
	</div>
	<div class="modal" data-popup="popup-bacero01">
		<div class="modal-content">
			<div class="modal-header">
				<a class="popup-back" href="#" data-popup-open="popup-bacero03" data-popup-close="popup-bacero01"><</a>
				<a class="popup-next" href="#" data-popup-open="popup-bacero02" data-popup-close="popup-bacero01">></a>
				<a class="popup-close" data-popup-close="popup-bacero01" href="#">x</a>
				<h2>Barandas de acero inoxidable</h2>
			</div>
		    <div class="modal-body">
			    <section class="row poproductos">
			    	<div class="col col6">
			    		<img src="images/bacero01.jpg" alt="">
			    	</div>
			    	<div class="col col6">
			    		<h4>Descripción</h4>
			    		<p>Un estilo y belleza únicos. Es un producto moderno e innovador</p>
			    	</div>
			    </section>
		    </div>
		</div>
	</div>
	<div class="modal" data-popup="popup-bacero02">
		<div class="modal-content">
			<div class="modal-header">
				<a class="popup-back" href="#" data-popup-open="popup-bacero01" data-popup-close="popup-bacero02"><</a>
				<a class="popup-next" href="#" data-popup-open="popup-bacero03" data-popup-close="popup-bacero02">></a>
				<a class="popup-close" data-popup-close="popup-bacero02" href="#">x</a>
				<h2>Barandas de acero inoxidable</h2>
			</div>
		    <div class="modal-body">
			    <section class="row poproductos">
			    	<div class="col col6">
			    		<img src="images/bacero02.jpg" alt="">
			    	</div>
			    	<div class="col col6">
			    		<h4>Descripción</h4>
			    		<p>Un estilo y belleza únicos. Es un producto moderno e innovador</p>
			    	</div>
			    </section>
		    </div>
		</div>
	</div>
	<div class="modal" data-popup="popup-bacero03">
		<div class="modal-content">
			<div class="modal-header">
				<a class="popup-back" href="#" data-popup-open="popup-bacero02" data-popup-close="popup-bacero03"><</a>
				<a class="popup-next" href="#" data-popup-open="popup-bacero01" data-popup-close="popup-bacero03">></a>
				<a class="popup-close" data-popup-close="popup-bacero03" href="#">x</a>
				<h2>Barandas de acero inoxidable</h2>
			</div>
		    <div class="modal-body">
			    <section class="row poproductos">
			    	<div class="col col6">
			    		<img src="images/bacero03.jpg" alt="">
			    	</div>
			    	<div class="col col6">
			    		<h4>Descripción</h4>
			    		<p>Un estilo y belleza únicos. Es un producto moderno e innovador</p>
			    	</div>
			    </section>
		    </div>
		</div>
	</div>
<?php include 'include/footer.php'; ?>
