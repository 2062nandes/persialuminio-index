window.onload = function () {
	new Vue({
		el: '#app',
		data: {
			formSubmitted: false,
			vue: {
				exp: null,
				nombre: '',
				telefono:'',
				movil:'',
				requeridos:'',
				medidas:'',
				ciudad:'',
				direccion:'',
				email:'',
				mensaje:'',
				envio: ''
			}
		},
		created: function(){
			// this.consultarMail();
		},
		methods: {
			consultarMail: function(){
				this.$http.get('mail.php').then(function(response){
							this.vue.envio = response.data;
							// console.log(response);
						}, function(){
				});
			},
			isFormValid: function(){
				return this.nombre != '';
			},
			submitForm: function(){
				if(!this.isFormValid()) return;
				this.formSubmitted = true;
				// this.consultarMail();
				this.$http.post('mail.php',{ vue: this.vue }).then(function(response){
							this.vue.envio = response.data;
							// console.log(response);
						}, function(){
				});
			},
			clearForm: function(){
				this.vue.nombre = '';
				this.vue.telefono = '';
				this.vue.movil = '';
				this.vue.ciudad = '';
				this.vue.requeridos = '';
				this.vue.medidas = '';
				this.vue.direccion = '';
				this.vue.email = '';
				this.vue.mensaje = '';
				this.vue.envio = '';
				this.formSubmitted = false;
			}
		}
	});
}
$(document).on('ready',inicio);
function inicio(){
	$('[data-popup-open]').on('click', productos);
	$('[data-popup-close]').on('click', close);
}
function close(e){
	var targ = $(this).attr('data-popup-close');
	$('[data-popup= "' +  targ +'"]').fadeOut(350);
	e.preventDefault();
}
function productos(e){
	var targ = $(this).attr('data-popup-open');
	$('[data-popup="' + targ + '"]').fadeIn(350);
	e.preventDefault();
}
