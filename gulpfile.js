const gulp = require('gulp'),
      /*optimizar imagenes*/
      os = require('os'),
      rename = require('gulp-rename'),
      concurrentTransform = require('concurrent-transform'),
      changed = require('gulp-changed'),
      imagemin = require('gulp-imagemin'),
      pngquant = require('imagemin-pngquant'),
      imageResize = require('gulp-image-resize'),
      cpus = os.cpus().length,

      // Gulp Plugins
      sass = require('gulp-sass'), //Preprocesador CSS
      postcss = require('gulp-postcss'), //Transpilador CSS

     // PostCSS Plugins
      cssnano = require('cssnano'),// Minificacion de css
      pxtorem = require('postcss-pxtorem');// Convertir valores de px a rem basado en baseFontSize

let postCssPlugins = [
  cssnano({
    autoprefixer: {
      add: true,
      browsers: 'last 2 versions'
    },// add autoprefijos css
    // discardComments: false,// se desactivó para manipular comentarios para sass
     core: false // se desactivó para manipular outputStyle para sass
  }),
  pxtorem
];

let resizeAndMinImagesTask = function(options){
  options['imagemin'] = {
    progressive: true,
    interlaced: true,
    svgoPlugins: [{
      removeViewBox: false //https://github.com/svg/svgo/issues/3
    }],
    use: [
      pngquant()
    ]
  }
  return function () {
      return gulp.src("./docs/imgs/**/*.{jpg,jpeg,gif,png,svg}")
      .pipe(rename(function (path) {
        path.basename += "-" + options.width;
      }))
      .pipe(changed("./public_html/images"))
      .pipe(concurrentTransform(imageResize({width: options.width}), cpus))
      .pipe(imagemin(options["imagemin"]))
      .pipe(gulp.dest("./public_html/images"));
    }
};
gulp.task("agropecuaria", resizeAndMinImagesTask({width: 300}));
gulp.task("images", ["agropecuaria"]);
gulp.task('img', function () {
  gulp.watch("./docs/imgs/**/*.{jpg,jpeg,gif,png,svg}", ['images']);
});

gulp.task('sass', ()=>
  gulp.src('./css/style.scss')
    .pipe(sass({

      // sourceComments: true
    }))
    .pipe(postcss(postCssPlugins))
    .pipe(gulp.dest('./css'))
);
// gulp.task('pug', () =>
//   gulp.src('./pug/*.pug')
//     .pipe(pug({
//       pretty:true
//     }))
//     .pipe(gulp.dest('./public_html/'))
// );
gulp.task('default', () => {
  gulp.watch('./css/**/*.scss',['sass']);
});
