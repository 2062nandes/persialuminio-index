<?php include 'include/header.php'; ?>
	<section class="row productos">
		<div class="col12 titulo">
			<h3>Ventanas</h3>
		</div>
		<div class="col12">
			<h3>Ventanas corredizas</h3>
		</div>
			<figure>
				<a href="#" data-popup-open="popup-vcorredizas01"><img src="images/vcorredizas01.jpg" alt=""></a>
				<figcaption>
					<h3>Ventanas corredizas</h3>
					<a href="#" data-popup-open="popup-vcorredizas01">Ver más</a>
				</figcaption>
			</figure>
			<figure>
				<a href="#" data-popup-open="popup-vcorredizas02"><img src="images/vcorredizas02.jpg" alt=""></a>
				<figcaption>
					<h3>Ventanas corredizas</h3>
					<a href="#" data-popup-open="popup-vcorredizas02">Ver más</a>
				</figcaption>
			</figure>
			<figure>
				<a href="#" data-popup-open="popup-vcorredizas03"><img src="images/vcorredizas03.jpg" alt=""></a>
				<figcaption>
					<h3>Ventanas corredizas</h3>
					<a href="#" data-popup-open="popup-vcorredizas03">Ver más</a>
				</figcaption>
			</figure>
		<div class="col12">
			<h3>Ventanas proyectantes</h3>
		</div>
			<figure>
				<a href="#" data-popup-open="popup-vproyectantes01"><img src="images/vproyectantes01.jpg" alt=""></a>
				<figcaption>
					<h3>Ventanas proyectantes</h3>
					<a href="#" data-popup-open="popup-vproyectantes01">Ver más</a>
				</figcaption>
			</figure>
			<figure>
				<a href="#" data-popup-open="popup-vproyectantes02"><img src="images/vproyectantes02.jpg" alt=""></a>
				<figcaption>
					<h3>Ventanas proyectantes</h3>
					<a href="#" data-popup-open="popup-vproyectantes02">Ver más</a>
				</figcaption>
			</figure>
			<figure>
				<a href="#" data-popup-open="popup-vproyectantes03"><img src="images/vproyectantes03.jpg" alt=""></a>
				<figcaption>
					<h3>Ventanas proyectantes</h3>
					<a href="#" data-popup-open="popup-vproyectantes03">Ver más</a>
				</figcaption>
			</figure>
		<div class="col12">
			<h3>Ventanas basculantes</h3>
		</div>
			<figure>
				<a href="#" data-popup-open="popup-vbasculantes01"><img src="images/vbasculantes01.jpg" alt=""></a>
				<figcaption>
					<h3>Ventanas basculantes</h3>
					<a href="#" data-popup-open="popup-vbasculantes01">Ver más</a>
				</figcaption>
			</figure>
			<figure>
				<a href="#" data-popup-open="popup-vbasculantes02"><img src="images/vbasculantes02.jpg" alt=""></a>
				<figcaption>
					<h3>Ventanas basculantes</h3>
					<a href="#" data-popup-open="popup-vbasculantes02">Ver más</a>
				</figcaption>
			</figure>
			<figure>
				<a href="#" data-popup-open="popup-vbasculantes03"><img src="images/vbasculantes03.jpg" alt=""></a>
				<figcaption>
					<h3>Ventanas basculantes</h3>
					<a href="#" data-popup-open="popup-vbasculantes03">Ver más</a>
				</figcaption>
			</figure>
		<div class="col12">
			<h3>Ventanas con celosía</h3>
		</div>
			<figure>
				<a href="#" data-popup-open="popup-vcelosia01"><img src="images/vcelosia01.jpg" alt=""></a>
				<figcaption>
					<h3>Ventanas con celosía</h3>
					<a href="#" data-popup-open="popup-vcelosia01">Ver más</a>
				</figcaption>
			</figure>
			<figure>
				<a href="#" data-popup-open="popup-vcelosia02"><img src="images/vcelosia02.jpg" alt=""></a>
				<figcaption>
					<h3>Ventanas con celosía</h3>
					<a href="#" data-popup-open="popup-vcelosia02">Ver más</a>
				</figcaption>
			</figure>
			<figure>
				<a href="#" data-popup-open="popup-vcelosia03"><img src="images/vcelosia03.jpg" alt=""></a>
				<figcaption>
					<h3>Ventanas con celosía</h3>
					<a href="#" data-popup-open="popup-vcelosia03">Ver más</a>
				</figcaption>
			</figure>
		<div class="col12">
			<h3>Ventanas - fijas</h3>
		</div>
			<figure>
				<a href="#" data-popup-open="popup-vfijas01"><img src="images/vfijas01.jpg" alt=""></a>
				<figcaption>
					<h3>Puertas fijas</h3>
					<a href="#" data-popup-open="popup-vfijas01">Ver más</a>
				</figcaption>
			</figure>
			<figure>
				<a href="#" data-popup-open="popup-vfijas02"><img src="images/vfijas02.jpg" alt=""></a>
				<figcaption>
					<h3>Puertas fijas</h3>
					<a href="#" data-popup-open="popup-vfijas02">Ver más</a>
				</figcaption>
			</figure>
			<figure>
				<a href="#" data-popup-open="popup-vfijas03"><img src="images/vfijas03.jpg" alt=""></a>
				<figcaption>
					<h3>Puertas fijas</h3>
					<a href="#" data-popup-open="popup-vfijas03">Ver más</a>
				</figcaption>
			</figure>
	</section>
	<div class="modal" data-popup="popup-vcorredizas01">
		<div class="modal-content">
			<div class="modal-header">
				<a class="popup-back" href="#" data-popup-open="popup-vcorredizas03" data-popup-close="popup-vcorredizas01"><</a>
				<a class="popup-next" href="#" data-popup-open="popup-vcorredizas02" data-popup-close="popup-vcorredizas01">></a>
				<a class="popup-close" data-popup-close="popup-vcorredizas01" href="#">x</a>
				<h2>Ventanas corredizas</h2>
			</div>
		    <div class="modal-body">
			    <section class="row poproductos">
			    	<div class="col col6">
			    		<img src="images/vcorredizas01.jpg" alt="">
			    	</div>
			    	<div class="col col6">
			    		<h4>Descripción</h4>
			    		<p>La ventanas corredizas está especialmente indicada cuando se dispone de un espacio reducido</p>
			    	</div>
			    </section>
		    </div>
		</div>
	</div>
	<div class="modal" data-popup="popup-vcorredizas02">
		<div class="modal-content">
			<div class="modal-header">
				<a class="popup-back" href="#" data-popup-open="popup-vcorredizas01" data-popup-close="popup-vcorredizas02"><</a>
				<a class="popup-next" href="#" data-popup-open="popup-vcorredizas03" data-popup-close="popup-vcorredizas02">></a>
				<a class="popup-close" data-popup-close="popup-vcorredizas02" href="#">x</a>
				<h2>Ventanas corredizas</h2>
			</div>
		    <div class="modal-body">
			    <section class="row poproductos">
			    	<div class="col col6">
			    		<img src="images/vcorredizas02.jpg" alt="">
			    	</div>
			    	<div class="col col6">
			    		<h4>Descripción</h4>
			    		<p>La ventanas corredizas está especialmente indicada cuando se dispone de un espacio reducido</p>
			    	</div>
			    </section>
		    </div>
		</div>
	</div>
	<div class="modal" data-popup="popup-vcorredizas03">
		<div class="modal-content">
			<div class="modal-header">
				<a class="popup-back" href="#" data-popup-open="popup-vcorredizas02" data-popup-close="popup-vcorredizas03"><</a>
				<a class="popup-next" href="#" data-popup-open="popup-vcorredizas01" data-popup-close="popup-vcorredizas03">></a>
				<a class="popup-close" data-popup-close="popup-vcorredizas03" href="#">x</a>
				<h2>Ventanas corredizas</h2>
			</div>
		    <div class="modal-body">
			    <section class="row poproductos">
			    	<div class="col col6">
			    		<img src="images/vcorredizas03.jpg" alt="">
			    	</div>
			    	<div class="col col6">
			    		<h4>Descripción</h4>
			    		<p>La ventanas corredizas está especialmente indicada cuando se dispone de un espacio reducido</p>
			    	</div>
			    </section>
		    </div>
		</div>
	</div>
	<div class="modal" data-popup="popup-vproyectantes01">
		<div class="modal-content">
			<div class="modal-header">
				<a class="popup-back" href="#" data-popup-open="popup-vproyectantes03" data-popup-close="popup-vproyectantes01"><</a>
				<a class="popup-next" href="#" data-popup-open="popup-vproyectantes02" data-popup-close="popup-vproyectantes01">></a>
				<a class="popup-close" data-popup-close="popup-vproyectantes01" href="#">x</a>
				<h2>Ventanas proyectantes</h2>
			</div>
		    <div class="modal-body">
			    <section class="row poproductos">
			    	<div class="col col6">
			    		<img src="images/vproyectantes01.jpg" alt="">
			    	</div>
			    	<div class="col col6">
			    		<h4>Descripción</h4>
			    		<p>La ventanas proyectantes está especialmente indicada cuando se dispone de un espacio reducido</p>
			    	</div>
			    </section>
		    </div>
		</div>
	</div>
	<div class="modal" data-popup="popup-vproyectantes02">
		<div class="modal-content">
			<div class="modal-header">
				<a class="popup-back" href="#" data-popup-open="popup-vproyectantes01" data-popup-close="popup-vproyectantes02"><</a>
				<a class="popup-next" href="#" data-popup-open="popup-vproyectantes03" data-popup-close="popup-vproyectantes02">></a>
				<a class="popup-close" data-popup-close="popup-vproyectantes02" href="#">x</a>
				<h2>Ventanas proyectantes</h2>
			</div>
		    <div class="modal-body">
			    <section class="row poproductos">
			    	<div class="col col6">
			    		<img src="images/vproyectantes02.jpg" alt="">
			    	</div>
			    	<div class="col col6">
			    		<h4>Descripción</h4>
			    		<p>La ventanas proyectantes está especialmente indicada cuando se dispone de un espacio reducido</p>
			    	</div>
			    </section>
		    </div>
		</div>
	</div>
	<div class="modal" data-popup="popup-vproyectantes03">
		<div class="modal-content">
			<div class="modal-header">
				<a class="popup-back" href="#" data-popup-open="popup-vproyectantes02" data-popup-close="popup-vproyectantes03"><</a>
				<a class="popup-next" href="#" data-popup-open="popup-vproyectantes01" data-popup-close="popup-vproyectantes03">></a>
				<a class="popup-close" data-popup-close="popup-vproyectantes03" href="#">x</a>
				<h2>Ventanas proyectantes</h2>
			</div>
		    <div class="modal-body">
			    <section class="row poproductos">
			    	<div class="col col6">
			    		<img src="images/vproyectantes03.jpg" alt="">
			    	</div>
			    	<div class="col col6">
			    		<h4>Descripción</h4>
			    		<p>La ventanas proyectantes está especialmente indicada cuando se dispone de un espacio reducido</p>
			    	</div>
			    </section>
		    </div>
		</div>
	</div>
	<div class="modal" data-popup="popup-vbasculantes01">
		<div class="modal-content">
			<div class="modal-header">
				<a class="popup-back" href="#" data-popup-open="popup-vbasculantes03" data-popup-close="popup-vbasculantes01"><</a>
				<a class="popup-next" href="#" data-popup-open="popup-vbasculantes02" data-popup-close="popup-vbasculantes01">></a>
				<a class="popup-close" data-popup-close="popup-vbasculantes01" href="#">x</a>
				<h2>Ventanas basculantes</h2>
			</div>
		    <div class="modal-body">
			    <section class="row poproductos">
			    	<div class="col col6">
			    		<img src="images/vbasculantes01.jpg" alt="">
			    	</div>
			    	<div class="col col6">
			    		<h4>Descripción</h4>
			    		<p>La ventanas basculantes está especialmente indicada cuando se dispone de un espacio reducido</p>
			    	</div>
			    </section>
		    </div>
		</div>
	</div>
	<div class="modal" data-popup="popup-vbasculantes02">
		<div class="modal-content">
			<div class="modal-header">
				<a class="popup-back" href="#" data-popup-open="popup-vbasculantes01" data-popup-close="popup-vbasculantes02"><</a>
				<a class="popup-next" href="#" data-popup-open="popup-vbasculantes03" data-popup-close="popup-vbasculantes02">></a>
				<a class="popup-close" data-popup-close="popup-vbasculantes02" href="#">x</a>
				<h2>Ventanas basculantes</h2>
			</div>
		    <div class="modal-body">
			    <section class="row poproductos">
			    	<div class="col col6">
			    		<img src="images/vbasculantes02.jpg" alt="">
			    	</div>
			    	<div class="col col6">
			    		<h4>Descripción</h4>
			    		<p>La ventanas basculantes está especialmente indicada cuando se dispone de un espacio reducido</p>
			    	</div>
			    </section>
		    </div>
		</div>
	</div>
	<div class="modal" data-popup="popup-vbasculantes03">
		<div class="modal-content">
			<div class="modal-header">
				<a class="popup-back" href="#" data-popup-open="popup-vbasculantes02" data-popup-close="popup-vbasculantes03"><</a>
				<a class="popup-next" href="#" data-popup-open="popup-vbasculantes01" data-popup-close="popup-vbasculantes03">></a>
				<a class="popup-close" data-popup-close="popup-vbasculantes03" href="#">x</a>
				<h2>Ventanas basculantes</h2>
			</div>
		    <div class="modal-body">
			    <section class="row poproductos">
			    	<div class="col col6">
			    		<img src="images/vbasculantes03.jpg" alt="">
			    	</div>
			    	<div class="col col6">
			    		<h4>Descripción</h4>
			    		<p>La ventanas basculantes está especialmente indicada cuando se dispone de un espacio reducido</p>
			    	</div>
			    </section>
		    </div>
		</div>
	</div>
	<div class="modal" data-popup="popup-vcelosia01">
		<div class="modal-content">
			<div class="modal-header">
				<a class="popup-back" href="#" data-popup-open="popup-vcelosia03" data-popup-close="popup-vcelosia01"><</a>
				<a class="popup-next" href="#" data-popup-open="popup-vcelosia02" data-popup-close="popup-vcelosia01">></a>
				<a class="popup-close" data-popup-close="popup-vcelosia01" href="#">x</a>
				<h2>Ventanas celosia</h2>
			</div>
		    <div class="modal-body">
			    <section class="row poproductos">
			    	<div class="col col6">
			    		<img src="images/vcelosia01.jpg" alt="">
			    	</div>
			    	<div class="col col6">
			    		<h4>Descripción</h4>
			    		<p>La ventanas celosia está especialmente indicada cuando se dispone de un espacio reducido</p>
			    	</div>
			    </section>
		    </div>
		</div>
	</div>
	<div class="modal" data-popup="popup-vcelosia02">
		<div class="modal-content">
			<div class="modal-header">
				<a class="popup-back" href="#" data-popup-open="popup-vcelosia01" data-popup-close="popup-vcelosia02"><</a>
				<a class="popup-next" href="#" data-popup-open="popup-vcelosia03" data-popup-close="popup-vcelosia02">></a>
				<a class="popup-close" data-popup-close="popup-vcelosia02" href="#">x</a>
				<h2>Ventanas celosia</h2>
			</div>
		    <div class="modal-body">
			    <section class="row poproductos">
			    	<div class="col col6">
			    		<img src="images/vcelosia02.jpg" alt="">
			    	</div>
			    	<div class="col col6">
			    		<h4>Descripción</h4>
			    		<p>La ventanas celosia está especialmente indicada cuando se dispone de un espacio reducido</p>
			    	</div>
			    </section>
		    </div>
		</div>
	</div>
	<div class="modal" data-popup="popup-vcelosia03">
		<div class="modal-content">
			<div class="modal-header">
				<a class="popup-back" href="#" data-popup-open="popup-vcelosia02" data-popup-close="popup-vcelosia03"><</a>
				<a class="popup-next" href="#" data-popup-open="popup-vcelosia01" data-popup-close="popup-vcelosia03">></a>
				<a class="popup-close" data-popup-close="popup-vcelosia03" href="#">x</a>
				<h2>Ventanas celosia</h2>
			</div>
		    <div class="modal-body">
			    <section class="row poproductos">
			    	<div class="col col6">
			    		<img src="images/vcelosia03.jpg" alt="">
			    	</div>
			    	<div class="col col6">
			    		<h4>Descripción</h4>
			    		<p>La ventanas celosia está especialmente indicada cuando se dispone de un espacio reducido</p>
			    	</div>
			    </section>
		    </div>
		</div>
	</div>
	<div class="modal" data-popup="popup-vfijas01">
		<div class="modal-content">
			<div class="modal-header">
				<a class="popup-back" href="#" data-popup-open="popup-vfijas03" data-popup-close="popup-vfijas01"><</a>
				<a class="popup-next" href="#" data-popup-open="popup-vfijas02" data-popup-close="popup-vfijas01">></a>
				<a class="popup-close" data-popup-close="popup-vfijas01" href="#">x</a>
				<h2>Ventanas fijas</h2>
			</div>
		    <div class="modal-body">
			    <section class="row poproductos">
			    	<div class="col col6">
			    		<img src="images/vfijas01.jpg" alt="">
			    	</div>
			    	<div class="col col6">
			    		<h4>Descripción</h4>
			    		<p>La ventanas fijas está especialmente indicada cuando se dispone de un espacio reducido</p>
			    	</div>
			    </section>
		    </div>
		</div>
	</div>
	<div class="modal" data-popup="popup-vfijas02">
		<div class="modal-content">
			<div class="modal-header">
				<a class="popup-back" href="#" data-popup-open="popup-vfijas01" data-popup-close="popup-vfijas02"><</a>
				<a class="popup-next" href="#" data-popup-open="popup-vfijas03" data-popup-close="popup-vfijas02">></a>
				<a class="popup-close" data-popup-close="popup-vfijas02" href="#">x</a>
				<h2>Ventanas fijas</h2>
			</div>
		    <div class="modal-body">
			    <section class="row poproductos">
			    	<div class="col col6">
			    		<img src="images/vfijas02.jpg" alt="">
			    	</div>
			    	<div class="col col6">
			    		<h4>Descripción</h4>
			    		<p>La ventanas fijas está especialmente indicada cuando se dispone de un espacio reducido</p>
			    	</div>
			    </section>
		    </div>
		</div>
	</div>
	<div class="modal" data-popup="popup-vfijas03">
		<div class="modal-content">
			<div class="modal-header">
				<a class="popup-back" href="#" data-popup-open="popup-vfijas02" data-popup-close="popup-vfijas03"><</a>
				<a class="popup-next" href="#" data-popup-open="popup-vfijas01" data-popup-close="popup-vfijas03">></a>
				<a class="popup-close" data-popup-close="popup-vfijas03" href="#">x</a>
				<h2>Ventanas fijas</h2>
			</div>
		    <div class="modal-body">
			    <section class="row poproductos">
			    	<div class="col col6">
			    		<img src="images/vfijas03.jpg" alt="">
			    	</div>
			    	<div class="col col6">
			    		<h4>Descripción</h4>
			    		<p>La ventanas fijas está especialmente indicada cuando se dispone de un espacio reducido</p>
			    	</div>
			    </section>
		    </div>
		</div>
	</div>
<?php include 'include/footer.php'; ?>
