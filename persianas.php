<?php include 'include/header.php'; ?>
	<section class="row productos">
		<div class="col12">
			<h3>Persianas black out</h3>
		</div>
			<figure>
				<a href="#" data-popup-open="popup-pblackout01"><img src="images/pblackout01.jpg" alt=""></a>
				<figcaption>
					<h3>Persianas black out</h3>
					<a href="#" data-popup-open="popup-pblackout01">Ver más</a>
				</figcaption>
			</figure>
			<figure>
				<a href="#" data-popup-open="popup-pblackout02"><img src="images/pblackout02.jpg" alt=""></a>
				<figcaption>
					<h3>Persianas black out</h3>
					<a href="#" data-popup-open="popup-pblackout02">Ver más</a>
				</figcaption>
			</figure>
			<figure>
				<a href="#" data-popup-open="popup-pblackout03"><img src="images/pblackout03.jpg" alt=""></a>
				<figcaption>
					<h3>Persianas black out</h3>
					<a href="#" data-popup-open="popup-pblackout03">Ver más</a>
				</figcaption>
			</figure>
		<div class="col12">
			<h3>Persianas eclipse</h3>
		</div>
			<figure>
				<a href="#" data-popup-open="popup-peclipse01"><img src="images/peclipse01.jpg" alt=""></a>
				<figcaption>
					<h3>Persianas eclipse</h3>
					<a href="#" data-popup-open="popup-peclipse01">Ver más</a>
				</figcaption>
			</figure>
			<figure>
				<a href="#" data-popup-open="popup-peclipse02"><img src="images/peclipse02.jpg" alt=""></a>
				<figcaption>
					<h3>Persianas eclipse</h3>
					<a href="#" data-popup-open="popup-peclipse02">Ver más</a>
				</figcaption>
			</figure>
			<figure>
				<a href="#" data-popup-open="popup-peclipse03"><img src="images/peclipse03.jpg" alt=""></a>
				<figcaption>
					<h3>Persianas eclipse</h3>
					<a href="#" data-popup-open="popup-peclipse03">Ver más</a>
				</figcaption>
			</figure>
		<div class="col12">
			<h3>Persianas verticales</h3>
		</div>
			<figure>
				<a href="#" data-popup-open="popup-pverticales01"><img src="images/pverticales01.jpg" alt=""></a>
				<figcaption>
					<h3>Persianas verticales</h3>
					<a href="#" data-popup-open="popup-pverticales01">Ver más</a>
				</figcaption>
			</figure>
			<figure>
				<a href="#" data-popup-open="popup-pverticales02"><img src="images/pverticales02.jpg" alt=""></a>
				<figcaption>
					<h3>Persianas verticales</h3>
					<a href="#" data-popup-open="popup-pverticales02">Ver más</a>
				</figcaption>
			</figure>
			<figure>
				<a href="#" data-popup-open="popup-pverticales03"><img src="images/pverticales03.jpg" alt=""></a>
				<figcaption>
					<h3>Persianas verticales</h3>
					<a href="#" data-popup-open="popup-pverticales03">Ver más</a>
				</figcaption>
			</figure>
		<div class="col12">
			<h3>Persianas horizontales</h3>
		</div>
			<figure>
				<a href="#" data-popup-open="popup-phorizontales01"><img src="images/phorizontales01.jpg" alt=""></a>
				<figcaption>
					<h3>Persianas horizontales</h3>
					<a href="#" data-popup-open="popup-phorizontales01">Ver más</a>
				</figcaption>
			</figure>
			<figure>
				<a href="#" data-popup-open="popup-phorizontales02"><img src="images/phorizontales02.jpg" alt=""></a>
				<figcaption>
					<h3>Persianas horizontales</h3>
					<a href="#" data-popup-open="popup-phorizontales02">Ver más</a>
				</figcaption>
			</figure>
			<figure>
				<a href="#" data-popup-open="popup-phorizontales03"><img src="images/phorizontales03.jpg" alt=""></a>
				<figcaption>
					<h3>Persianas horizontales</h3>
					<a href="#" data-popup-open="popup-phorizontales03">Ver más</a>
				</figcaption>
			</figure>
	</section>
	<!--
 		Persianas black out pblackout01
        Persianas eclipse peclipse02
        Persianas verticales pverticales03
        Persianas horizontales phorizontales03
	-->
	<div class="modal" data-popup="popup-pblackout01">
		<div class="modal-content">
			<div class="modal-header">
				<a class="popup-back" href="#" data-popup-open="popup-pblackout03" data-popup-close="popup-pblackout01"><</a>
				<a class="popup-next" href="#" data-popup-open="popup-pblackout02" data-popup-close="popup-pblackout01">></a>
				<a class="popup-close" data-popup-close="popup-pblackout01" href="#">x</a>
				<h2>Persianas black out</h2>
			</div>
		    <div class="modal-body">
			    <section class="row poproductos">
			    	<div class="col col6">
			    		<img src="images/pblackout01.jpg" alt="">
			    	</div>
			    	<div class="col col6">
			    		<h4>Descripción</h4>
						<p>Propios persianas de fabricación proporcionando al cliente los diseños más diversos, materiales y colores.</p>
			    	</div>
			    </section>
		    </div>
		</div>
	</div>
	<div class="modal" data-popup="popup-pblackout02">
		<div class="modal-content">
			<div class="modal-header">
				<a class="popup-back" href="#" data-popup-open="popup-pblackout01" data-popup-close="popup-pblackout02"><</a>
				<a class="popup-next" href="#" data-popup-open="popup-pblackout03" data-popup-close="popup-pblackout02">></a>
				<a class="popup-close" data-popup-close="popup-pblackout02" href="#">x</a>
				<h2>Persianas black out</h2>
			</div>
		    <div class="modal-body">
			    <section class="row poproductos">
			    	<div class="col col6">
			    		<img src="images/pblackout02.jpg" alt="">
			    	</div>
			    	<div class="col col6">
			    		<h4>Descripción</h4>
						<p>Propios persianas de fabricación proporcionando al cliente los diseños más diversos, materiales y colores.</p>
			    	</div>
			    </section>
		    </div>
		</div>
	</div>
	<div class="modal" data-popup="popup-pblackout03">
		<div class="modal-content">
			<div class="modal-header">
				<a class="popup-back" href="#" data-popup-open="popup-pblackout02" data-popup-close="popup-pblackout03"><</a>
				<a class="popup-next" href="#" data-popup-open="popup-pblackout01" data-popup-close="popup-pblackout03">></a>
				<a class="popup-close" data-popup-close="popup-pblackout03" href="#">x</a>
				<h2>Persianas black out</h2>
			</div>
		    <div class="modal-body">
			    <section class="row poproductos">
			    	<div class="col col6">
			    		<img src="images/pblackout03.jpg" alt="">
			    	</div>
			    	<div class="col col6">
			    		<h4>Descripción</h4>
						<p>Propios persianas de fabricación proporcionando al cliente los diseños más diversos, materiales y colores.</p>
			    	</div>
			    </section>
		    </div>
		</div>
	</div>
	<div class="modal" data-popup="popup-peclipse01">
		<div class="modal-content">
			<div class="modal-header">
				<a class="popup-back" href="#" data-popup-open="popup-peclipse03" data-popup-close="popup-peclipse01"><</a>
				<a class="popup-next" href="#" data-popup-open="popup-peclipse02" data-popup-close="popup-peclipse01">></a>
				<a class="popup-close" data-popup-close="popup-peclipse01" href="#">x</a>
				<h2>Persianas eclipse</h2>
			</div>
		    <div class="modal-body">
			    <section class="row poproductos">
			    	<div class="col col6">
			    		<img src="images/peclipse01.jpg" alt="">
			    	</div>
			    	<div class="col col6">
			    		<h4>Descripción</h4>
						<p>La cortina de interior eclipse es una línea de vanguardia que combina dos diseños en uno. Cuando está abierto provee una visión clara del exterior.</p>
			    	</div>
			    </section>
		    </div>
		</div>
	</div>
	<div class="modal" data-popup="popup-peclipse02">
		<div class="modal-content">
			<div class="modal-header">
				<a class="popup-back" href="#" data-popup-open="popup-peclipse01" data-popup-close="popup-peclipse02"><</a>
				<a class="popup-next" href="#" data-popup-open="popup-peclipse03" data-popup-close="popup-peclipse02">></a>
				<a class="popup-close" data-popup-close="popup-peclipse02" href="#">x</a>
				<h2>Persianas eclipse</h2>
			</div>
		    <div class="modal-body">
			    <section class="row poproductos">
			    	<div class="col col6">
			    		<img src="images/peclipse02.jpg" alt="">
			    	</div>
			    	<div class="col col6">
			    		<h4>Descripción</h4>
						<p>La cortina de interior eclipse es una línea de vanguardia que combina dos diseños en uno. Cuando está abierto provee una visión clara del exterior.</p>
			    	</div>
			    </section>
		    </div>
		</div>
	</div>
	<div class="modal" data-popup="popup-peclipse03">
		<div class="modal-content">
			<div class="modal-header">
				<a class="popup-back" href="#" data-popup-open="popup-peclipse02" data-popup-close="popup-peclipse03"><</a>
				<a class="popup-next" href="#" data-popup-open="popup-peclipse01" data-popup-close="popup-peclipse03">></a>
				<a class="popup-close" data-popup-close="popup-peclipse03" href="#">x</a>
				<h2>Persianas eclipse</h2>
			</div>
		    <div class="modal-body">
			    <section class="row poproductos">
			    	<div class="col col6">
			    		<img src="images/peclipse03.jpg" alt="">
			    	</div>
			    	<div class="col col6">
			    		<h4>Descripción</h4>
						<p>La cortina de interior eclipse es una línea de vanguardia que combina dos diseños en uno. Cuando está abierto provee una visión clara del exterior.</p>
			    	</div>
			    </section>
		    </div>
		</div>
	</div>
	<div class="modal" data-popup="popup-pverticales01">
		<div class="modal-content">
			<div class="modal-header">
				<a class="popup-back" href="#" data-popup-open="popup-pverticales03" data-popup-close="popup-pverticales01"><</a>
				<a class="popup-next" href="#" data-popup-open="popup-pverticales02" data-popup-close="popup-pverticales01">></a>
				<a class="popup-close" data-popup-close="popup-pverticales01" href="#">x</a>
				<h2>Persianas verticales</h2>
			</div>
		    <div class="modal-body">
			    <section class="row poproductos">
			    	<div class="col col6">
			    		<img src="images/pverticales01.jpg" alt="">
			    	</div>
			    	<div class="col col6">
			    		<h4>Descripción</h4>
						<p>Este sistema de accionamiento manual, está disponible en una gran variedad de texturas y colores</p>
			    	</div>
			    </section>
		    </div>
		</div>
	</div>
	<div class="modal" data-popup="popup-pverticales02">
		<div class="modal-content">
			<div class="modal-header">
				<a class="popup-back" href="#" data-popup-open="popup-pverticales01" data-popup-close="popup-pverticales02"><</a>
				<a class="popup-next" href="#" data-popup-open="popup-pverticales03" data-popup-close="popup-pverticales02">></a>
				<a class="popup-close" data-popup-close="popup-pverticales02" href="#">x</a>
				<h2>Persianas verticales</h2>
			</div>
		    <div class="modal-body">
			    <section class="row poproductos">
			    	<div class="col col6">
			    		<img src="images/pverticales02.jpg" alt="">
			    	</div>
			    	<div class="col col6">
			    		<h4>Descripción</h4>
						<p>Este sistema de accionamiento manual, está disponible en una gran variedad de texturas y colores</p>
			    	</div>
			    </section>
		    </div>
		</div>
	</div>
	<div class="modal" data-popup="popup-pverticales03">
		<div class="modal-content">
			<div class="modal-header">
				<a class="popup-back" href="#" data-popup-open="popup-pverticales02" data-popup-close="popup-pverticales03"><</a>
				<a class="popup-next" href="#" data-popup-open="popup-pverticales01" data-popup-close="popup-pverticales03">></a>
				<a class="popup-close" data-popup-close="popup-pverticales03" href="#">x</a>
				<h2>Persianas verticales</h2>
			</div>
		    <div class="modal-body">
			    <section class="row poproductos">
			    	<div class="col col6">
			    		<img src="images/pverticales03.jpg" alt="">
			    	</div>
			    	<div class="col col6">
			    		<h4>Descripción</h4>
						<p>Este sistema de accionamiento manual, está disponible en una gran variedad de texturas y colores</p>
			    	</div>
			    </section>
		    </div>
		</div>
	</div>
	<div class="modal" data-popup="popup-phorizontales01">
		<div class="modal-content">
			<div class="modal-header">
				<a class="popup-back" href="#" data-popup-open="popup-phorizontales03" data-popup-close="popup-phorizontales01"><</a>
				<a class="popup-next" href="#" data-popup-open="popup-phorizontales02" data-popup-close="popup-phorizontales01">></a>
				<a class="popup-close" data-popup-close="popup-phorizontales01" href="#">x</a>
				<h2>Persianas horizontales</h2>
			</div>
		    <div class="modal-body">
			    <section class="row poproductos">
			    	<div class="col col6">
			    		<img src="images/phorizontales01.jpg" alt="">
			    	</div>
			    	<div class="col col6">
			    		<h4>Descripción</h4>
						<p>Este sistema de accionamiento manual, está disponible en una gran variedad de texturas y colores</p>
			    	</div>
			    </section>
		    </div>
		</div>
	</div>
	<div class="modal" data-popup="popup-phorizontales02">
		<div class="modal-content">
			<div class="modal-header">
				<a class="popup-back" href="#" data-popup-open="popup-phorizontales01" data-popup-close="popup-phorizontales02"><</a>
				<a class="popup-next" href="#" data-popup-open="popup-phorizontales03" data-popup-close="popup-phorizontales02">></a>
				<a class="popup-close" data-popup-close="popup-phorizontales02" href="#">x</a>
				<h2>Persianas horizontales</h2>
			</div>
		    <div class="modal-body">
			    <section class="row poproductos">
			    	<div class="col col6">
			    		<img src="images/phorizontales02.jpg" alt="">
			    	</div>
			    	<div class="col col6">
			    		<h4>Descripción</h4>
						<p>Este sistema de accionamiento manual, está disponible en una gran variedad de texturas y colores</p>
			    	</div>
			    </section>
		    </div>
		</div>
	</div>
	<div class="modal" data-popup="popup-phorizontales03">
		<div class="modal-content">
			<div class="modal-header">
				<a class="popup-back" href="#" data-popup-open="popup-phorizontales02" data-popup-close="popup-phorizontales03"><</a>
				<a class="popup-next" href="#" data-popup-open="popup-phorizontales01" data-popup-close="popup-phorizontales03">></a>
				<a class="popup-close" data-popup-close="popup-phorizontales03" href="#">x</a>
				<h2>Persianas horizontales</h2>
			</div>
		    <div class="modal-body">
			    <section class="row poproductos">
			    	<div class="col col6">
			    		<img src="images/phorizontales03.jpg" alt="">
			    	</div>
			    	<div class="col col6">
			    		<h4>Descripción</h4>
						<p>Este sistema de accionamiento manual, está disponible en una gran variedad de texturas y colores</p>
			    	</div>
			    </section>
		    </div>
		</div>
	</div>
<?php include 'include/footer.php'; ?>
