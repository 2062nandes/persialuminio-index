<?php include 'include/header.php'; ?>
	<section class="row productos">
		<div class="col12">
			<h3>Boxes de blindex</h3>
		</div>
			<figure>
				<a href="#" data-popup-open="bblindex01"><img src="images/bblindex01.jpg" alt=""></a>
				<figcaption>
					<h3>Box de blindex</h3>
					<a href="#" data-popup-open="bblindex01">Ver más</a>
				</figcaption>
			</figure>
			<figure>
				<a href="#" data-popup-open="bblindex02"><img src="images/bblindex02.jpg" alt=""></a>
				<figcaption>
					<h3>Box de blindex</h3>
					<a href="#" data-popup-open="bblindex02">Ver más</a>
				</figcaption>
			</figure>
			<figure>
				<a href="#" data-popup-open="bblindex03"><img src="images/bblindex03.jpg" alt=""></a>
				<figcaption>
					<h3>Box de blindex</h3>
					<a href="#" data-popup-open="bblindex03">Ver más</a>
				</figcaption>
			</figure>
		<div class="col12">
			<h3>Boxes de acrílico</h3>
		</div>
			<figure>
				<a href="#" data-popup-open="bacrilico01"><img src="images/bacrilico01.jpg" alt=""></a>
				<figcaption>
					<h3>Box de acrílico</h3>
					<a href="#" data-popup-open="bacrilico01">Ver más</a>
				</figcaption>
			</figure>
			<figure>
				<a href="#" data-popup-open="bacrilico02"><img src="images/bacrilico02.jpg" alt=""></a>
				<figcaption>
					<h3>Box de acrílico</h3>
					<a href="#" data-popup-open="bacrilico02">Ver más</a>
				</figcaption>
			</figure>
			<figure>
				<a href="#" data-popup-open="bacrilico03"><img src="images/bacrilico03.jpg" alt=""></a>
				<figcaption>
					<h3>Box de acrílico</h3>
					<a href="#" data-popup-open="bacrilico03">Ver más</a>
				</figcaption>
			</figure>
		<div class="col12">
			<h3>Espejos sencillos</h3>
		</div>
			<figure>
				<a href="#" data-popup-open="esencillos01"><img src="images/esencillos01.jpg" alt=""></a>
				<figcaption>
					<h3>Espejos sencillos</h3>
					<a href="#" data-popup-open="esencillos01">Ver más</a>
				</figcaption>
			</figure>
			<figure>
				<a href="#" data-popup-open="esencillos02"><img src="images/esencillos02.jpg" alt=""></a>
				<figcaption>
					<h3>Espejos sencillos</h3>
					<a href="#" data-popup-open="esencillos02">Ver más</a>
				</figcaption>
			</figure>
			<figure>
				<a href="#" data-popup-open="esencillos03"><img src="images/esencillos03.jpg" alt=""></a>
				<figcaption>
					<h3>Espejos sencillos</h3>
					<a href="#" data-popup-open="esencillos03">Ver más</a>
				</figcaption>
			</figure>
		<div class="col12">
			<h3>Espejos bicelados</h3>
		</div>
			<figure>
				<a href="#" data-popup-open="ebicelados01"><img src="images/ebicelados01.jpg" alt=""></a>
				<figcaption>
					<h3>Espejos bicelados</h3>
					<a href="#" data-popup-open="ebicelados01">Ver más</a>
				</figcaption>
			</figure>
			<figure>
				<a href="#" data-popup-open="ebicelados02"><img src="images/ebicelados02.jpg" alt=""></a>
				<figcaption>
					<h3>Espejos bicelados</h3>
					<a href="#" data-popup-open="ebicelados02">Ver más</a>
				</figcaption>
			</figure>
			<figure>
				<a href="#" data-popup-open="ebicelados03"><img src="images/ebicelados03.jpg" alt=""></a>
				<figcaption>
					<h3>Espejos bicelados</h3>
					<a href="#" data-popup-open="ebicelados03">Ver más</a>
				</figcaption>
			</figure>
	</section>
	<div class="modal" data-popup="bblindex01">
		<div class="modal-content">
			<div class="modal-header">
				<a class="popup-back" href="#" data-popup-open="bblindex03" data-popup-close="bblindex01"><</a>
				<a class="popup-next" href="#" data-popup-open="bblindex02" data-popup-close="bblindex01">></a>
				<a class="popup-close" data-popup-close="bblindex01" href="#">x</a>
				<h2>Boxes de blindex</h2>
			</div>
		    <div class="modal-body">
			    <section class="row poproductos">
			    	<div class="col col6">
			    		<img src="images/bblindex01.jpg" alt="">
			    	</div>
			    	<div class="col col6">
			    		<h4>Descripción</h4>
			    		<p>La apreciación de todos los detalles, comodidad, practicidad y seguridad son los mejores atributos que comercializa Caja de blindex servicio puede ofrecer a su cuarto de baño. espacios pequeños se pueden optimizar con un diseño inteligente y moderno. Aclimatarse baños más pequeños con la elegancia y el buen gusto es nuestro trabajo.</p>
			    	</div>
			    </section>
		    </div>
		</div>
	</div>
	<div class="modal" data-popup="bblindex02">
		<div class="modal-content">
			<div class="modal-header">
				<a class="popup-back" href="#" data-popup-open="bblindex01" data-popup-close="bblindex02"><</a>
				<a class="popup-next" href="#" data-popup-open="bblindex03" data-popup-close="bblindex02">></a>
				<a class="popup-close" data-popup-close="bblindex02" href="#">x</a>
				<h2>Boxes de blindex</h2>
			</div>
		    <div class="modal-body">
			    <section class="row poproductos">
			    	<div class="col col6">
			    		<img src="images/bblindex02.jpg" alt="">
			    	</div>
			    	<div class="col col6">
			    		<h4>Descripción</h4>
			    		<p>La apreciación de todos los detalles, comodidad, practicidad y seguridad son los mejores atributos que comercializa Caja de blindex servicio puede ofrecer a su cuarto de baño. espacios pequeños se pueden optimizar con un diseño inteligente y moderno. Aclimatarse baños más pequeños con la elegancia y el buen gusto es nuestro trabajo.</p>
			    	</div>
			    </section>
		    </div>
		</div>
	</div>
	<div class="modal" data-popup="bblindex03">
		<div class="modal-content">
			<div class="modal-header">
				<a class="popup-back" href="#" data-popup-open="bblindex02" data-popup-close="bblindex03"><</a>
				<a class="popup-next" href="#" data-popup-open="bblindex01" data-popup-close="bblindex03">></a>
				<a class="popup-close" data-popup-close="bblindex03" href="#">x</a>
				<h2>Boxes de blindex</h2>
			</div>
		    <div class="modal-body">
			    <section class="row poproductos">
			    	<div class="col col6">
			    		<img src="images/bblindex03.jpg" alt="">
			    	</div>
			    	<div class="col col6">
			    		<h4>Descripción</h4>
			    		<p>La apreciación de todos los detalles, comodidad, practicidad y seguridad son los mejores atributos que comercializa Caja de blindex servicio puede ofrecer a su cuarto de baño. espacios pequeños se pueden optimizar con un diseño inteligente y moderno. Aclimatarse baños más pequeños con la elegancia y el buen gusto es nuestro trabajo.</p>
			    	</div>
			    </section>
		    </div>
		</div>
	</div>
	<div class="modal" data-popup="bacrilico01">
		<div class="modal-content">
			<div class="modal-header">
				<a class="popup-back" href="#" data-popup-open="bacrilico03" data-popup-close="bacrilico01"><</a>
				<a class="popup-next" href="#" data-popup-open="bacrilico02" data-popup-close="bacrilico01">></a>
				<a class="popup-close" data-popup-close="bacrilico01" href="#">x</a>
				<h2>Boxes de acrílico</h2>
			</div>
		    <div class="modal-body">
			    <section class="row poproductos">
			    	<div class="col col6">
			    		<img src="images/bacrilico01.jpg" alt="">
			    	</div>
			    	<div class="col col6">
			    		<h4>Descripción</h4>
			    		<p>La apreciación de todos los detalles, comodidad, practicidad y seguridad son los mejores atributos que comercializa Caja de acrílico servicio puede ofrecer a su cuarto de baño. espacios pequeños se pueden optimizar con un diseño inteligente y moderno. Aclimatarse baños más pequeños con la elegancia y el buen gusto es nuestro trabajo.</p>
			    	</div>
			    </section>
		    </div>
		</div>
	</div>
	<div class="modal" data-popup="bacrilico02">
		<div class="modal-content">
			<div class="modal-header">
				<a class="popup-back" href="#" data-popup-open="bacrilico01" data-popup-close="bacrilico02"><</a>
				<a class="popup-next" href="#" data-popup-open="bacrilico03" data-popup-close="bacrilico02">></a>
				<a class="popup-close" data-popup-close="bacrilico02" href="#">x</a>
				<h2>Boxes de acrílico</h2>
			</div>
		    <div class="modal-body">
			    <section class="row poproductos">
			    	<div class="col col6">
			    		<img src="images/bacrilico02.jpg" alt="">
			    	</div>
			    	<div class="col col6">
			    		<h4>Descripción</h4>
			    		<p>La apreciación de todos los detalles, comodidad, practicidad y seguridad son los mejores atributos que comercializa Caja de acrílico servicio puede ofrecer a su cuarto de baño. espacios pequeños se pueden optimizar con un diseño inteligente y moderno. Aclimatarse baños más pequeños con la elegancia y el buen gusto es nuestro trabajo.</p>
			    	</div>
			    </section>
		    </div>
		</div>
	</div>
	<div class="modal" data-popup="bacrilico03">
		<div class="modal-content">
			<div class="modal-header">
				<a class="popup-back" href="#" data-popup-open="bacrilico02" data-popup-close="bacrilico03"><</a>
				<a class="popup-next" href="#" data-popup-open="bacrilico01" data-popup-close="bacrilico03">></a>
				<a class="popup-close" data-popup-close="bacrilico03" href="#">x</a>
				<h2>Boxes de acrílico</h2>
			</div>
		    <div class="modal-body">
			    <section class="row poproductos">
			    	<div class="col col6">
			    		<img src="images/bacrilico03.jpg" alt="">
			    	</div>
			    	<div class="col col6">
			    		<h4>Descripción</h4>
			    		<p>La apreciación de todos los detalles, comodidad, practicidad y seguridad son los mejores atributos que comercializa Caja de acrílico servicio puede ofrecer a su cuarto de baño. espacios pequeños se pueden optimizar con un diseño inteligente y moderno. Aclimatarse baños más pequeños con la elegancia y el buen gusto es nuestro trabajo.</p>
			    	</div>
			    </section>
		    </div>
		</div>
	</div>
	<div class="modal" data-popup="esencillos01">
		<div class="modal-content">
			<div class="modal-header">
				<a class="popup-back" href="#" data-popup-open="esencillos03" data-popup-close="esencillos01"><</a>
				<a class="popup-next" href="#" data-popup-open="esencillos02" data-popup-close="esencillos01">></a>
				<a class="popup-close" data-popup-close="esencillos01" href="#">x</a>
				<h2>Espejos sencillos</h2>
			</div>
		    <div class="modal-body">
			    <section class="row poproductos">
			    	<div class="col col6">
			    		<img src="images/esencillos01.jpg" alt="">
			    	</div>
			    	<div class="col col6">
			    		<h4>Descripción</h4>
			    		<p>Los espejos están de moda. Son la tendencia de las nuevas casas, en todos los estilos.</p>
			    	</div>
			    </section>
		    </div>
		</div>
	</div>
	<div class="modal" data-popup="esencillos02">
		<div class="modal-content">
			<div class="modal-header">
				<a class="popup-back" href="#" data-popup-open="esencillos01" data-popup-close="esencillos02"><</a>
				<a class="popup-next" href="#" data-popup-open="esencillos03" data-popup-close="esencillos02">></a>
				<a class="popup-close" data-popup-close="esencillos02" href="#">x</a>
				<h2>Espejos sencillos</h2>
			</div>
		    <div class="modal-body">
			    <section class="row poproductos">
			    	<div class="col col6">
			    		<img src="images/esencillos02.jpg" alt="">
			    	</div>
			    	<div class="col col6">
			    		<h4>Descripción</h4>
			    		<p>Los espejos están de moda. Son la tendencia de las nuevas casas, en todos los estilos.</p>
			    	</div>
			    </section>
		    </div>
		</div>
	</div>
	<div class="modal" data-popup="esencillos03">
		<div class="modal-content">
			<div class="modal-header">
				<a class="popup-back" href="#" data-popup-open="esencillos02" data-popup-close="esencillos03"><</a>
				<a class="popup-next" href="#" data-popup-open="esencillos01" data-popup-close="esencillos03">></a>
				<a class="popup-close" data-popup-close="esencillos03" href="#">x</a>
				<h2>Espejos sencillos</h2>
			</div>
		    <div class="modal-body">
			    <section class="row poproductos">
			    	<div class="col col6">
			    		<img src="images/esencillos03.jpg" alt="">
			    	</div>
			    	<div class="col col6">
			    		<h4>Descripción</h4>
			    		<p>Los espejos están de moda. Son la tendencia de las nuevas casas, en todos los estilos.</p>
			    	</div>
			    </section>
		    </div>
		</div>
	</div>
	<div class="modal" data-popup="ebicelados01">
		<div class="modal-content">
			<div class="modal-header">
				<a class="popup-back" href="#" data-popup-open="ebicelados03" data-popup-close="ebicelados01"><</a>
				<a class="popup-next" href="#" data-popup-open="ebicelados02" data-popup-close="ebicelados01">></a>
				<a class="popup-close" data-popup-close="ebicelados01" href="#">x</a>
				<h2>Espejos bicelados</h2>
			</div>
		    <div class="modal-body">
			    <section class="row poproductos">
			    	<div class="col col6">
			    		<img src="images/ebicelados01.jpg" alt="">
			    	</div>
			    	<div class="col col6">
			    		<h4>Descripción</h4>
			    		<p>Los espejos están de moda. Son la tendencia de las nuevas casas, en todos los estilos.</p>
			    	</div>
			    </section>
		    </div>
		</div>
	</div>
	<div class="modal" data-popup="ebicelados02">
		<div class="modal-content">
			<div class="modal-header">
				<a class="popup-back" href="#" data-popup-open="ebicelados01" data-popup-close="ebicelados02"><</a>
				<a class="popup-next" href="#" data-popup-open="ebicelados03" data-popup-close="ebicelados02">></a>
				<a class="popup-close" data-popup-close="ebicelados02" href="#">x</a>
				<h2>Espejos bicelados</h2>
			</div>
		    <div class="modal-body">
			    <section class="row poproductos">
			    	<div class="col col6">
			    		<img src="images/ebicelados02.jpg" alt="">
			    	</div>
			    	<div class="col col6">
			    		<h4>Descripción</h4>
			    		<p>Los espejos están de moda. Son la tendencia de las nuevas casas, en todos los estilos.</p>
			    	</div>
			    </section>
		    </div>
		</div>
	</div>
	<div class="modal" data-popup="ebicelados03">
		<div class="modal-content">
			<div class="modal-header">
				<a class="popup-back" href="#" data-popup-open="ebicelados2" data-popup-close="ebicelados03"><</a>
				<a class="popup-next" href="#" data-popup-open="ebicelados01" data-popup-close="ebicelados03">></a>
				<a class="popup-close" data-popup-close="ebicelados03" href="#">x</a>
				<h2>Espejos bicelados</h2>
			</div>
		    <div class="modal-body">
			    <section class="row poproductos">
			    	<div class="col col6">
			    		<img src="images/ebicelados03.jpg" alt="">
			    	</div>
			    	<div class="col col6">
			    		<h4>Descripción</h4>
			    		<p>Los espejos están de moda. Son la tendencia de las nuevas casas, en todos los estilos.</p>
			    	</div>
			    </section>
		    </div>
		</div>
	</div>
<?php include 'include/footer.php'; ?>
