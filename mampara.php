<?php include 'include/header.php'; ?>
	<section class="row productos">
		<div class="col12">
			<h3>Mamparas de Eucatex</h3>
		</div>
			<figure>
				<a href="#" data-popup-open="popup-meucatex01"><img src="images/meucatex01.jpg" alt=""></a>
				<figcaption>
					<h3>Mamparas de eucatex</h3>
					<a href="#" data-popup-open="popup-meucatex01">Ver más</a>
				</figcaption>
			</figure>
			<figure>
				<a href="#" data-popup-open="popup-meucatex02"><img src="images/meucatex02.jpg" alt=""></a>
				<figcaption>
					<h3>Mamparas de eucatex</h3>
					<a href="#" data-popup-open="popup-meucatex02">Ver más</a>
				</figcaption>
			</figure>
			<figure>
				<a href="#" data-popup-open="popup-meucatex03"><img src="images/meucatex03.jpg" alt=""></a>
				<figcaption>
					<h3>Mamparas de eucatex</h3>
					<a href="#" data-popup-open="popup-meucatex03">Ver más</a>
				</figcaption>
			</figure>
		<div class="col12">
			<h3>Mamparas de melamina</h3>
		</div>
			<figure>
				<a href="#" data-popup-open="popup-mmelamina01"><img src="images/mmelamina01.jpg" alt=""></a>
				<figcaption>
					<h3>Divisiones de melamina</h3>
					<a href="#" data-popup-open="popup-mmelamina01">Ver más</a>
				</figcaption>
			</figure>
			<figure>
				<a href="#" data-popup-open="popup-mmelamina02"><img src="images/mmelamina02.jpg" alt=""></a>
				<figcaption>
					<h3>Divisiones de melamina</h3>
					<a href="#" data-popup-open="popup-mmelamina02">Ver más</a>
				</figcaption>
			</figure>
			<figure>
				<a href="#" data-popup-open="popup-mmelamina03"><img src="images/mmelamina03.jpg" alt=""></a>
				<figcaption>
					<h3>Divisiones de melamina</h3>
					<a href="#" data-popup-open="popup-mmelamina03">Ver más</a>
				</figcaption>
			</figure>
		<div class="col12">
			<h3>Mamparas de blindex</h3>
		</div>
			<figure>
				<a href="#" data-popup-open="popup-mblindex01"><img src="images/mblindex01.jpg" alt=""></a>
				<figcaption>
					<h3>Divisiones de blindex</h3>
					<a href="#" data-popup-open="popup-mblindex01">Ver más</a>
				</figcaption>
			</figure>
			<figure>
				<a href="#" data-popup-open="popup-mblindex02"><img src="images/mblindex02.jpg" alt=""></a>
				<figcaption>
					<h3>Divisiones de blindex</h3>
					<a href="#" data-popup-open="popup-mblindex02">Ver más</a>
				</figcaption>
			</figure>
			<figure>
				<a href="#" data-popup-open="popup-mblindex03"><img src="images/mblindex03.jpg" alt=""></a>
				<figcaption>
					<h3>Divisiones de blindex</h3>
					<a href="#" data-popup-open="popup-mblindex03">Ver más</a>
				</figcaption>
			</figure>
	</section>
	<!--
 		 Mamparas de Eucatex
         Mamparas de melamina
         Mamparas de blindex
	-->
	<div class="modal" data-popup="popup-meucatex01">
		<div class="modal-content">
			<div class="modal-header">
				<a class="popup-back" href="#" data-popup-open="popup-meucatex03" data-popup-close="popup-meucatex01"><</a>
				<a class="popup-next" href="#" data-popup-open="popup-meucatex02" data-popup-close="popup-meucatex01">></a>
				<a class="popup-close" data-popup-close="popup-meucatex01" href="#">x</a>
				<h2>Mamparas de eucatex</h2>
			</div>
		    <div class="modal-body">
			    <section class="row poproductos">
			    	<div class="col col6">
			    		<img src="images/meucatex01.jpg" alt="">
			    	</div>
			    	<div class="col col6">
			    		<h4>Descripción</h4>
			    		<p>Realizamos divisorias de placas eucatex para oficinas,consultorios,tinglados. </p>
			    	</div>
			    </section>
		    </div>
		</div>
	</div>
	<div class="modal" data-popup="popup-meucatex02">
		<div class="modal-content">
			<div class="modal-header">
				<a class="popup-back" href="#" data-popup-open="popup-meucatex01" data-popup-close="popup-meucatex02"><</a>
				<a class="popup-next" href="#" data-popup-open="popup-meucatex03" data-popup-close="popup-meucatex02">></a>
				<a class="popup-close" data-popup-close="popup-meucatex02" href="#">x</a>
				<h2>Mamparas de eucatex</h2>
			</div>
		    <div class="modal-body">
			    <section class="row poproductos">
			    	<div class="col col6">
			    		<img src="images/meucatex02.jpg" alt="">
			    	</div>
			    	<div class="col col6">
			    		<h4>Descripción</h4>
			    		<p>Realizamos divisorias de placas eucatex para oficinas,consultorios,tinglados. </p>
			    	</div>
			    </section>
		    </div>
		</div>
	</div>
	<div class="modal" data-popup="popup-meucatex03">
		<div class="modal-content">
			<div class="modal-header">
				<a class="popup-back" href="#" data-popup-open="popup-meucatex02" data-popup-close="popup-meucatex03"><</a>
				<a class="popup-next" href="#" data-popup-open="popup-meucatex01" data-popup-close="popup-meucatex03">></a>
				<a class="popup-close" data-popup-close="popup-meucatex03" href="#">x</a>
				<h2>Mamparas de eucatex</h2>
			</div>
		    <div class="modal-body">
			    <section class="row poproductos">
			    	<div class="col col6">
			    		<img src="images/meucatex03.jpg" alt="">
			    	</div>
			    	<div class="col col6">
			    		<h4>Descripción</h4>
			    		<p>Realizamos divisorias de placas eucatex para oficinas,consultorios,tinglados. </p>
			    	</div>
			    </section>
		    </div>
		</div>
	</div>
	<div class="modal" data-popup="popup-mmelamina01">
		<div class="modal-content">
			<div class="modal-header">
				<a class="popup-back" href="#" data-popup-open="popup-mmelamina03" data-popup-close="popup-mmelamina01"><</a>
				<a class="popup-next" href="#" data-popup-open="popup-mmelamina02" data-popup-close="popup-mmelamina01">></a>
				<a class="popup-close" data-popup-close="popup-mmelamina01" href="#">x</a>
				<h2>Divisiones de melamina</h2>
			</div>
		    <div class="modal-body">
			    <section class="row poproductos">
			    	<div class="col col6">
			    		<img src="images/mmelamina01.jpg" alt="">
			    	</div>
			    	<div class="col col6">
			    		<h4>Descripción</h4>
			    		<p>Realizamos divisorias de placas melamina para oficinas,consultorios. </p>
			    	</div>
			    </section>
		    </div>
		</div>
	</div>
	<div class="modal" data-popup="popup-mmelamina02">
		<div class="modal-content">
			<div class="modal-header">
				<a class="popup-back" href="#" data-popup-open="popup-mmelamina01" data-popup-close="popup-mmelamina02"><</a>
				<a class="popup-next" href="#" data-popup-open="popup-mmelamina03" data-popup-close="popup-mmelamina02">></a>
				<a class="popup-close" data-popup-close="popup-mmelamina02" href="#">x</a>
				<h2>Divisiones de melamina</h2>
			</div>
		    <div class="modal-body">
			    <section class="row poproductos">
			    	<div class="col col6">
			    		<img src="images/mmelamina02.jpg" alt="">
			    	</div>
			    	<div class="col col6">
			    		<h4>Descripción</h4>
			    		<p>Realizamos divisorias de placas melamina para oficinas,consultorios. </p>
			    	</div>
			    </section>
		    </div>
		</div>
	</div>
	<div class="modal" data-popup="popup-mmelamina03">
		<div class="modal-content">
			<div class="modal-header">
				<a class="popup-back" href="#" data-popup-open="popup-mmelamina02" data-popup-close="popup-mmelamina03"><</a>
				<a class="popup-next" href="#" data-popup-open="popup-mmelamina01" data-popup-close="popup-mmelamina03">></a>
				<a class="popup-close" data-popup-close="popup-mmelamina03" href="#">x</a>
				<h2>Divisiones de melamina</h2>
			</div>
		    <div class="modal-body">
			    <section class="row poproductos">
			    	<div class="col col6">
			    		<img src="images/mmelamina03.jpg" alt="">
			    	</div>
			    	<div class="col col6">
			    		<h4>Descripción</h4>
			    		<p>Realizamos divisorias de placas melamina para oficinas,consultorios. </p>
			    	</div>
			    </section>
		    </div>
		</div>
	</div>
	<div class="modal" data-popup="popup-mblindex01">
		<div class="modal-content">
			<div class="modal-header">
				<a class="popup-back" href="#" data-popup-open="popup-mblindex03" data-popup-close="popup-mblindex01"><</a>
				<a class="popup-next" href="#" data-popup-open="popup-mblindex02" data-popup-close="popup-mblindex01">></a>
				<a class="popup-close" data-popup-close="popup-mblindex01" href="#">x</a>
				<h2>Divisiones de blindex</h2>
			</div>
		    <div class="modal-body">
			    <section class="row poproductos">
			    	<div class="col col6">
			    		<img src="images/mblindex01.jpg" alt="">
			    	</div>
			    	<div class="col col6">
			    		<h4>Descripción</h4>
			    		<p>Realizamos divisorias de placas blindex para oficinas,consultorios. </p>
			    	</div>
			    </section>
		    </div>
		</div>
	</div>
	 <div class="modal" data-popup="popup-mblindex02">
		<div class="modal-content">
			<div class="modal-header">
				<a class="popup-back" href="#" data-popup-open="popup-mblindex01" data-popup-close="popup-mblindex02"><</a>
				<a class="popup-next" href="#" data-popup-open="popup-mblindex03" data-popup-close="popup-mblindex02">></a>
				<a class="popup-close" data-popup-close="popup-mblindex02" href="#">x</a>
				<h2>Divisiones de blindex</h2>
			</div>
		    <div class="modal-body">
			    <section class="row poproductos">
			    	<div class="col col6">
			    		<img src="images/mblindex02.jpg" alt="">
			    	</div>
			    	<div class="col col6">
			    		<h4>Descripción</h4>
			    		<p>Realizamos divisorias de placas blindex para oficinas,consultorios. </p>
			    	</div>
			    </section>
		    </div>
		</div>
	</div>
	<div class="modal" data-popup="popup-mblindex03">
		<div class="modal-content">
			<div class="modal-header">
				<a class="popup-back" href="#" data-popup-open="popup-mblindex02" data-popup-close="popup-mblindex03"><</a>
				<a class="popup-next" href="#" data-popup-open="popup-mblindex01" data-popup-close="popup-mblindex03">></a>
				<a class="popup-close" data-popup-close="popup-mblindex03" href="#">x</a>
				<h2>Divisiones de blindex</h2>
			</div>
		    <div class="modal-body">
			    <section class="row poproductos">
			    	<div class="col col6">
			    		<img src="images/mblindex03.jpg" alt="">
			    	</div>
			    	<div class="col col6">
			    		<h4>Descripción</h4>
			    		<p>Realizamos divisorias de placas blindex para oficinas,consultorios. </p>
			    	</div>
			    </section>
		    </div>
		</div>
	</div>
<?php include 'include/footer.php'; ?>
